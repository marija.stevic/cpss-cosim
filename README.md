# pn-cpss-cosim
Cyber-Physical-Social System Co-simulator for analysis of attack trees based on Petri Nets (PN) and Continuous-Time Markov Chains (CTMC).   
This repository represents software for the simulation phase of the CPSS Co-simulator.

## Installation

For standalone execution without docker environment check [Standalone](wiki/instructions.md#standalone-run) 
for Linux and [Standalone-windows](wiki/instructions.md#standalone-run-windows) for Windows.

### Installation (Ubuntu 16.04/18.04/20.04 )

Install docker and docker-compose. Please follow instaructions at,

https://docs.docker.com/engine/install/ubuntu/ and https://docs.docker.com/compose/install/ .

**Note**: The actions above require an internet-connection and admin access.  

After that you can copy(via WinSCP), git clone or download repository to an empty directory, 
then follow instructions at [Execution](#execution) section.

### Installation (Docker for Windows)
Prepare the docker-environment by following the installation instructions from:
https://docs.docker.com/docker-for-windows/install/

Allow the execution of Powershell-Scripts (.ps1), by executing in an
administrator PowerShell:

```powershell
Set-ExecutionPolicy remotesigned
```

![Open Docker-Settings](doc/windows/settings.png)

![Allow Drive-Access to C:](doc/windows/shareDrive.png)

Jump into the project directory. Then build the docker-image

```powershell
./build.ps1
```
**Note**: The actions above require an internet-connection.

The image can then be started via:
```powershell
./start.ps1
```

## Execution 

For production deployment i.e. along with other services, please refer to [Deployment](wiki/production_deploy.md).

Clone or extract contents of this repository to a directory(e.g. cosim_home). Then copy contents of 
copy_in_parent directory to the same directory. Directory structure should look like,  

cosim_home/  
__ all_configs.env  
__ docker-compose.yml  
__ pn-cpss-cosim(original repository directory)  

Follow the instructions at section [Installation](#installation).
After docker-compose is installed then navigate to coism_home in command line or terminal and run

command: **sudo docker-compose up --build**

To stop the application press Ctrl + C, then  

command: **sudo docker-compose down**

For standalone execution without docker environment check [Standalone](wiki/instructions.md#standalone-run) .  
This page also contains a scetion on how to run without docker on Windows at bottom. 

## Functionalities

#### f7

**Endpoint:** http://ip_addr:port/api/f7  
**Parsing logic:** src/parsers/jsonparser_in_F7.py  
**config properties:** props_f7 in config file

F7 api is used to predict probabilities based on current compromise event.
It returns a time series of probability values for all nodes in current tree.
Length of time series can be configured for each attack tree separately in
json_files/attack_tree_models/AttackModel_merged.json.

#### f8 

**Endpoint:** http://ip_addr:port/api/f8  
**Parsing logic:** src/parsers/jsonparser_in_F8.py  
**config properties:** props_f8 in config file

F8 api is used to measure effectiveness of countermeasure. 
It simulate in the same manner as F7 via CTMC but considers mitigations. 
It returns time series of of probability values for all nodes in current tree
along with probability of safe state.

## Configuration
all_configs.env is used to configure the behaviour
of the simulator. It is located in copy_in_parent folder and needs to be
copied to the same directory where repositiry was cloned into. This usually means one level up.
docker_compose.yml also needs to be copied.

### props_fX

props_f7 and props_f8 respectively used to specify properties of f7 and f8 endpoints.
They define four properties:

|Property | Description | Default value |
|---------|-------------|---------------|
|<pre>cosimulator__props_fX__in_foldername</pre> | input folder for fX endpoint(check Testing section below) | None |
|<pre>cosimulator__props_fX__out_endpoint</pre> | endpoint to which simulation result is sent | None |
|<pre>cosimulator__props_f7__out_response_type</pre> | string, file or json | json |
|<pre>cosimulator__props_f7__deactivate_endpoint</pre> | flag to deactivate server interface of fX | false |
|<pre>cosimulator__props_f7__output_path</pre> | location in container for storing f6 files | json_files/output_files/op_fX |

### Cache configurations

These properties are used to connect to cache service. They do not have any default value configured. 
For mode details how cache is used in cosimulator check [Persistence Layer](wiki/persistence layer.md).

|Property | Description | Example |
|---------|-------------|---------------|
|<pre>cosimulator__cache_host</pre>| cache service name as per docker-compose.yml | cache-cpss-cosim|
|<pre>cosimulator__cache_port</pre>| cache service port inside docker network |6379|
|<pre>cosimulator__cache_lifetime_in_seconds</pre>| expiry time of cached objects |3600|
|<pre>cosimulator__cache_max_retry</pre>| max no of retry if cache service is unreachable,  after that error is reported | 3|

### Other configurations 

These properties are also part of all_configs.env. But it is recommened not to change them.  

|Property | Description | Default value |
|---------|-------------|---------------|
|<pre>cosimulator__sim_debug</pre>| generate plots of probability values for all nodes |false|
|<pre>cosimulator__no_record</pre>| disable f6 file retention | false |
|<pre>cosimulator__reduced_record</pre>| disable original propbability time series retention  (before down-sampling)  | true |
|<pre>cosimulator__append_correlated_events</pre>| enable additionalInfo attribute from F7 to be appended in F6 | false |
|<pre>cosimulator__alternate_norm</pre>| use alternate normalization approach to get CDF | false |


### Logging configuration

The [logging-config-file](json_files/configs/log-config.json) is used to configure the behaviour
of the simulator.

| Property | Description | Default property |
|-----------|-------------|------------------|
| log_file_name | relative file path to log file | ../simulator_api.log |
| log_format | format of each log line | |
| max_log_size | log file max size in MB | 300MB |
| max_old_logs | max no of backup logs | 5 |
| log_level | level of detail in log | INFO |
| console_level | level of detail in print | DEBUG |
| enable_console_log | command line output enable? | False |

## API
If no test input is configured when executed as Flask app or in case executed
as Gunicorn app, an HTTP-Server spins up and serves a REST-ful API on port 8080.
The API-Endpoints and formats are described in the corresponding
[OpenAPI-YAML](doc/openapi.yaml).

It can be tested with curl (for example in git bash) by sending the input file
like so:
```bash
curl http://localhost:8080/api/f7 -T json_files/test_input_f7/valid/in_f7_T2_3.2_cEvent_ENG.json

curl http://localhost:8080/api/f8 -T json_files/test_input_f8/valid/in_f8_T2_3.2_cEvent_ENG.json
```
This call will instantly return `OK` (Status 202) if the JSON-input could be
parsed correctly.
The simulation will run asynchronuously in a separate thread, thus the
simulation-result is not reflected by this answer.

## Testing

Now it supports testing before application start up for both th API i.e. f7 and f8.
This can be used in case of execution as flask app, gunicorn app or in docker container.  
To use this feature, put the request json input in respective json_files/test_json_input_fX (X = 7 or 8) folder.  
Notice that there are two subfolders inside in both cases.

__'valid'__ : any possible test cases, assert successful start up of simulation of execution  
__'invalid'__ : currently only node not found test case is included, asserts trigger of exception  

It should be noted that all these files will be executed sequentially. The folder paths are
configurable in json_files/config.json as input_foldername in respective props_fX attribute.

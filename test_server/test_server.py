from flask import Flask, Blueprint, request, Response, make_response, current_app
import time
import  os
import json
import logging
import logging.handlers

JSON_MIME = "application/json; charset=utf-8"
test_server = Blueprint("test_server", __name__, url_prefix="/test_server")
OUTUT_FOLDER='./json_files'

counter=0

logger=None
log_config=None
test_server._got_first_request=False

first_request_ts=None

def setup_logger():

    if test_server._got_first_request==True:
        return

    global logger
    global log_config
    logger = logging.getLogger('test_srv_logger')
    
    logger.setLevel(logging.DEBUG)
    logging_levels=\
        {'INFO':logging.INFO,'DEBUG':logging.DEBUG,'ERROR':logging.ERROR}

    if log_config==None:
        log_config={}

    max_log_size = 250 * 1024 * 1024  # 250MB max log size default
    max_old_logs = 5  # max no of old log files
    log_level = 'DEBUG'  # default log level
    console_level = 'DEBUG'  # default print level
    #absolute path to log file
    log_file_name="./test_srv.log"
    flog=logging.handlers.RotatingFileHandler(log_file_name,
              mode='a', maxBytes=max_log_size, backupCount=max_old_logs)

    log_format = "[%(levelname)s] [%(asctime)s] [%(process)d] [%(threadName)s] : %(message)s"
    formatter = logging.Formatter(log_format)

    flog.setFormatter(formatter)
    flog.setLevel(logging_levels[log_level])
    logger.addHandler(flog)

    current_app.logger=logger
    test_server._got_first_request = True
    logger.info("logger configured")

test_server.before_request(setup_logger)

@test_server.route("")
def index():
    return Response("dummy server is up!!", 200, mimetype="text/plain")

@test_server.route("/count", methods=["POST", "PUT"])
def count():
	return Response(str(counter), 200, mimetype="application/json")


@test_server.route("/check", methods=["POST", "PUT"])
def response():
    
    global counter, first_request_ts
    counter=counter+1

    time_spent=0
    if first_request_ts==None:
        first_request_ts=time.time()
    else:
        time_spent=time.time()-first_request_ts

    # print("request rule: %s", request.url_rule)

    #logger.info("dummy server is hit!!!")
    logger.info("dummy server hit count: %d, first_request: [%s], time_spent: [%s]", \
                            counter, str(first_request_ts), str(time_spent))
    json_obj = request.get_json(force=True)

    # logger.info("Request received: %s", json.dumps(json_obj))
    # return Response(json.dumps(json_obj), 200, mimetype="application/json")
    return Response("{\"status\":\"ok\"}", 200, mimetype="application/json")

@test_server.route("/check_file", methods=["POST", "PUT"])
def response_file():

    logger.info("dummy server is hit with a file!!!")

    file = request.files['file']

    if file.filename != '':
        filename="dummyfile_"+str(time.time())+".json"
        file.save(os.path.join(OUTUT_FOLDER, filename))

    with open(os.path.join(OUTUT_FOLDER, filename), 'r') as fp:
        json_obj=fp.read()

    logger.info("Request received as file with content: %s", json_obj)
    return Response(json_obj, 200, mimetype="application/json")


if __name__=='__main__':
    app = Flask(__name__)
    app.register_blueprint(test_server)
    app.run("0.0.0.0", 9000, debug=True, use_reloader=False, threaded=True)  # processes=3



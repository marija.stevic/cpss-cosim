#include <iostream>
#include <vector>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>      

#include <boost/numeric/odeint.hpp>
#include <array>
#include <chrono> 

using namespace std::chrono; 
using namespace std;
using namespace boost::numeric::odeint;

typedef std::vector< float > state_type;


class attack_tree_aggregated_solver {

    int no_states, num_trans, num_mitigation_trans;
    int *to_states, *from_states;
    float *trans_rate;

    float *mitigation_transitions_rate;
    int *mitigation_transitions_to_states, *mitigation_transitions_from_states;

    int *enable_inter_safe_state_transition;

public:
    attack_tree_aggregated_solver(int no_states, int num_trans, int* to_states, int* from_states, float* trans_rate,
             int *mitigation_transitions_to_states, int *mitigation_transitions_from_states, 
             float *mitigation_transitions_rate, int num_mitigation_trans, int *enable_inter_safe_state_transition) {
        this->no_states = no_states;
        this->num_trans = num_trans;
        this->to_states = to_states;
        this->from_states = from_states;
        this->trans_rate = trans_rate;

        this->mitigation_transitions_rate = mitigation_transitions_rate;
        this->mitigation_transitions_to_states = mitigation_transitions_to_states;
        this->mitigation_transitions_from_states = mitigation_transitions_from_states;
        this->num_mitigation_trans = num_mitigation_trans;
        this->enable_inter_safe_state_transition = enable_inter_safe_state_transition;
     }

    void operator() ( const state_type &x , state_type &dxdt , const float /* t */ )
    {   
        float temp;
        for(int i=0; i<no_states;i++)
            dxdt[i] = 0.0;

        // includes self transtions adjustments for safe states only
        for(int i=0; i<num_trans-no_states/2; i++){
            //unsafe to unsafe
            dxdt[to_states[i]] += x[from_states[i]]*trans_rate[i];
            //safe to safe
            temp = enable_inter_safe_state_transition[i]*x[from_states[i]+no_states/2]*trans_rate[i];
            dxdt[to_states[i]+no_states/2] += temp;
            dxdt[from_states[i]+no_states/2] -= temp;
        }

        // self transtions adjustments for unsafe states only
        for(int i=num_trans-no_states/2; i<num_trans; i++){
            dxdt[to_states[i]] += x[from_states[i]]*trans_rate[i];
        }

         // unsafe to safe, safe state marking same marking as from state + safe state
        for(int i=0; i<num_mitigation_trans; i++){
            temp=x[mitigation_transitions_from_states[i]]*mitigation_transitions_rate[i];
            dxdt[mitigation_transitions_to_states[i]] += temp;
            //adjustment for additional transtions due to mitigation FROM unsafe states
            dxdt[mitigation_transitions_from_states[i]] -= temp;
        }

    }
};


struct save_probability_time_series
{
    float* result;
    int index, steps, no_states, num_places;
    char** aggregated_labels;
    char* mitigation_node_children;

    save_probability_time_series( float* result_t, int steps_t, int no_states_t, \
                    int num_places_t, char** aggregated_labels_t, char* mitigation_node_children_t){
        result=result_t;
        index=0;
        steps=steps_t;
        no_states=no_states_t;
        num_places=num_places_t;
        aggregated_labels=aggregated_labels_t;
        mitigation_node_children=mitigation_node_children_t;
    }

    void operator()( const state_type &x , double t )
    {   
        float sum_p=0.0;

        for(int s=0;s<no_states;s++)
            sum_p+=x[s];

        for(int p=0;p<num_places;p++)
            result[p*steps+index]=0.0;

        for(int s=0;s<no_states/2;s++){
            for(int p=0;p<num_places-1;p++){
                if(aggregated_labels[s][p]=='1')
                        result[p*steps+index]+=x[s];
                if(aggregated_labels[s][p]=='1' and mitigation_node_children[p]!='*')
                        result[p*steps+index]+=x[s+no_states/2];
            }
            result[(num_places-1)*steps+index]+=x[s+no_states/2];
        }

        for(int p=0;p<num_places;p++){
            result[p*steps+index]/=sum_p;
        }

        index++;
    }
};

extern "C" void main_func_f8(int no_states, int num_trans, int initial_state_index, 
    float final_time, int* to_states, int* from_states, float* individual_trans_rate, 
    int *transition_indexes, int *transition_indexes_len, int num_leaves, float *leaf_probabilities, 
    //length of atomic_mitigation_list
    int num_mitigation, 
    //index if mentioned in target_node_id
    int *target_places,
    //length of list(s) in target_node_id, length num_mitigation
    int *target_places_len,
    //length num_mitigation, efficiency/trans_rate
    float* individual_mitigation_rate, 
    char* mitigation_node_children,
    // num_places + 1 in python, result has space as F7 + safe at the end
    float* result, int num_places,
    char** aggregated_labels)
{

    srand (time(NULL));
    auto start = high_resolution_clock::now();

    float *trans_rate = new float[num_trans];
    int *enable_inter_safe_state_transition = new int[num_trans];
    for(int i=0; i<no_states; i++)
        trans_rate[num_trans-no_states+i]=0.0;

    for(int i=0; i<num_trans; i++)
        enable_inter_safe_state_transition[i]=1;

    for(int i=0, k=0; i<num_trans-no_states; i++){
        trans_rate[i]=0.0;
        for(int j=0; j<transition_indexes_len[i]; j++){
            trans_rate[i]+=individual_trans_rate[transition_indexes[k]];
            k++;
        }
        trans_rate[num_trans-no_states+from_states[i]]-=trans_rate[i];
    }

    state_type pi_0(no_states*2);
    for(int i=0; i<no_states*2;i++)
        pi_0[i] = 0.0;
    pi_0[initial_state_index] = 1.0;

    //only 1to1 correspondonce of from unsafe to safe state
    std::vector<int> mitigation_transitions_from_state;
    std::vector<int> mitigation_transitions_to_state;
    std::vector<float> mitigation_transitions_rate;

    int *target_places_ptr=target_places;
    for(int j=0; j<num_mitigation; j++){
        for(int k=0; k<target_places_len[j]; k++, target_places_ptr++){                
            int mit_to_index=-1;
            for(int p=0;p<num_trans;p++){
                if(aggregated_labels[to_states[p]][*target_places_ptr]=='1' 
                        && aggregated_labels[from_states[p]][*target_places_ptr]=='0'){
                    mit_to_index=from_states[p]+no_states;
                    mitigation_transitions_from_state.push_back(to_states[p]);
                    mitigation_transitions_to_state.push_back(mit_to_index);
                    mitigation_transitions_rate.push_back(individual_mitigation_rate[j]);
                    enable_inter_safe_state_transition[p]=0;
                }
            }
        }
    }
        
    //cout<<mitigation_node_children<<endl;
    cout<<"mitigation transtions: "<<mitigation_transitions_to_state.size()<<endl;

    /*for(int i=0;i<mitigation_transitions_to_state.size();i++)
        cout<<mitigation_transitions_from_state[i]<<" "<<mitigation_transitions_to_state[i]
                        <<" "<<mitigation_transitions_rate[i]<<endl;*/

    bulirsch_stoer_dense_out< state_type > stepper( 1E-9 , 1E-9 , 1.0 , 0.0 );

    double dt=1.0;
    int no_steps=(int) (final_time/dt);
    no_steps++;
    size_t steps = integrate_const(stepper, 
        attack_tree_aggregated_solver(no_states*2, num_trans, to_states, from_states, trans_rate, 
            mitigation_transitions_to_state.data(), mitigation_transitions_from_state.data(), 
            mitigation_transitions_rate.data(), mitigation_transitions_rate.size(), enable_inter_safe_state_transition) ,
        pi_0 , 0.0 , (double) final_time , dt ,
        save_probability_time_series( result, no_steps, 
            no_states*2, num_places, aggregated_labels, mitigation_node_children ) );

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start); 
  
    cout << "integrated in(us): " << duration.count() << endl;  

    float normalization_f;
    for(int p=0;p<num_leaves;p++){
        normalization_f=leaf_probabilities[p]/(result[p*no_steps]+0.001);
        for(int t=0; t<no_steps; t++)
            result[p*no_steps+t]*=normalization_f;
    } 

    /* output */
    /*int no_steps=(int) (final_time/dt);
    no_steps++;
    /*for(int i=0; i<no_states;i++)
    {   
        for( size_t t=0; t<no_steps; t++ )
            cout << result[i*no_steps+t] << '\t';
        cout<<"\n";
    }   

    cout<<"results: "<<endl;
    for(int i=0; i<5;i++)
        cout << result[num_places*no_steps-5+i] << '\t';
    cout<<endl;*/

    delete []trans_rate;
    delete []enable_inter_safe_state_transition;
    mitigation_transitions_from_state.clear();
    mitigation_transitions_to_state.clear();
    mitigation_transitions_rate.clear();

    return;

}

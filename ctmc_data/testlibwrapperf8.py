import ctypes
import random
import time
import json
import matplotlib.pyplot as plt
import ctmc_parser_utils

#testlib = ctypes.CDLL('/usr/samp/bigc.so')
testlib = ctypes.CDLL('/home/dd/Documents/ii/1103/bigc_f82.so')

def get_optimized_transition_lists(attree):

	optimized_transition_list=dict()
	leaf_probabilities_dict=dict()

	tree_id=attree["attackTree_ID"]

	leaf_probabilities=dict()
	for leaf in attree["leaf_nodes"]:
		leaf_probabilities[leaf["node_ID"]]=leaf["probability_leaf_node"]
	leaf_probabilities_dict=leaf_probabilities

	transitions=list()
	#[rate, leaf_node_ids]
	for trans in attree["transitions"]:
		t_list=list()
		t_list.append(1.0/trans['compromise_t'])
		for i in trans["inputPlaces"]:
			if i in leaf_probabilities:
				t_list.append(i)
		transitions.append(t_list)
	optimized_transition_list=transitions

	return [optimized_transition_list, leaf_probabilities_dict]

def create(optimized_transition_list, leaf_probabilities_dict):

	temp=list()
	transition_rate=(ctypes.c_float * len(optimized_transition_list))(*temp)
	for tr_i in range(len(optimized_transition_list)):
		transition_rate[tr_i]=0.0
		tr_entry=optimized_transition_list[tr_i]
		norm_rate=tr_entry[0] # rate
		if len(tr_entry)>1:
			for ii in tr_entry[1:]:

				##SKIP HERE IF EVENT

				if ii in leaf_probabilities_dict:
					norm_rate*=leaf_probabilities_dict[ii]
		transition_rate[tr_i] += norm_rate

	return transition_rate

ctmc_model=None
tree_name="T2"
target_node_ids=["T2_3.1", "T2_4", "T2_5"]
#target_node_ids=["G2_4.2", "G2_4.1", "G2_5"]
#target_node_ids=["R_1.3.1.1", "R_2.2.2.3", "R_2.1.2"]
with open("ctmc_"+tree_name+"_p.json") as file:
	ctmc_model = json.load(file)

attack_model=None
with open("AttackTree_"+tree_name+".json") as file:
	attack_model = json.load(file)

state_labels=ctmc_model["aggregate_state_labels"]
no_places=len(state_labels[0])

no_states=len(state_labels)
print(no_states)
num_trans=len(ctmc_model["aggregated_states_transitions"]["indexes"])
no_steps=51
endtime=50.0
initial_state_index=0

[optimized_transition_list, leaf_probabilities_dict]=get_optimized_transition_lists(attack_model["attackModel"][0])

num_mitigation=2
target_places=[]
for i in target_node_ids:
	target_places.append(ctmc_model["place_list"].index(i))
print(target_places)
target_places_len=[2, 1]
individual_mitigation_rate=[0.8/15, 0.9/20]


ctmc_parser_utils.populate_prefix_notation(attack_model["attackModel"][0])
label=ctmc_parser_utils.get_place_label(target_node_ids, ctmc_model["place_list"])
mitigation_node_children=ctmc_parser_utils.get_aggregated_label(label, ctmc_model["place_list"], tree_name)
for l in attack_model["attackModel"][0]['leaf_nodes']:
	i = ctmc_model["place_list"].index(l["node_ID"])
	if(mitigation_node_children[i]=='0'):
		mitigation_node_children[i]='1'
mitigation_node_children = "".join(mitigation_node_children)
print(mitigation_node_children)
mitigation_node_children=bytes(mitigation_node_children, encoding="utf-8")

target_places_arr=(ctypes.c_int * len(target_places))(*target_places)
target_places_len_arr=(ctypes.c_int * len(target_places_len))(*target_places_len)
individual_mitigation_rate_arr=(ctypes.c_float * len(individual_mitigation_rate))(*individual_mitigation_rate)

to_states=list()
from_states=list()

transition_indexes_len=list()
transition_indexes=list()
for i in range(num_trans):
	tr_k=ctmc_model["aggregated_states_transitions"]["indexes"][i]
	from_state=int(tr_k.split(",")[0].replace("(",""))
	to_state=int(tr_k.split(",")[1].replace(")",""))
	to_states.append(to_state)
	from_states.append(from_state)

	transition_indexes.extend(ctmc_model["aggregated_states_transitions"]["lists"][i])
	transition_indexes_len.append(len(ctmc_model["aggregated_states_transitions"]["lists"][i]))

for i in range(no_states):
	to_states.append(i)
	from_states.append(i)

trans_rate_arr=create(optimized_transition_list, leaf_probabilities_dict)

print(to_states[:4])
print(from_states[:4])
for i in range(5):
	print(trans_rate_arr[i])

start_time = time.perf_counter()
to_states_arr = (ctypes.c_int * len(to_states))(*to_states)
from_states_arr = (ctypes.c_int * len(from_states))(*from_states)
transition_indexes = (ctypes.c_int * len(transition_indexes))(*transition_indexes)
transition_indexes_len = (ctypes.c_int * len(transition_indexes_len))(*transition_indexes_len)
#trans_rate_arr = (ctypes.c_float * len(trans_rate))(*trans_rate)
res=list()
res_arr = (ctypes.c_float * ((no_places+1)*no_steps))(*res)
for i in range(len(state_labels)):
	state_labels[i]=bytes(state_labels[i], encoding="utf-8")
state_labels_arr = (ctypes.c_char_p * len(state_labels))(*state_labels)
convert_time = time.perf_counter() - start_time

f=testlib.main_func_f8
f.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_float, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), \
					ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), \
					ctypes.c_int, ctypes.POINTER(ctypes.c_float), \
					ctypes.c_int, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_float), \
					ctypes.c_char_p, ctypes.POINTER(ctypes.c_float), ctypes.c_int, ctypes.POINTER(ctypes.c_char_p))
#f.restype=ctypes.POINTER(ctypes.POINTER(ctypes.c_float * no_states))

num_leaves=len(leaf_probabilities_dict)
leaf_probabilities_list=list()
for i in leaf_probabilities_dict:
	leaf_probabilities_list.append(leaf_probabilities_dict[i])
leaf_probabilities_list_arr=(ctypes.c_float * len(leaf_probabilities_list))(*leaf_probabilities_list)

start_time = time.perf_counter()
convert_time = time.perf_counter() - start_time
f(no_states, num_trans+no_states, initial_state_index, endtime, \
	to_states_arr, from_states_arr, trans_rate_arr, transition_indexes, transition_indexes_len, \
	num_leaves, leaf_probabilities_list_arr, \
	num_mitigation, target_places_arr, target_places_len_arr, individual_mitigation_rate_arr, \
	mitigation_node_children, res_arr, no_places+1, state_labels_arr)
exec_time = time.perf_counter() - start_time

print(res_arr)
print(len(from_states_arr))
print("conversion time: "+str(convert_time))
print("exec_time: "+str(exec_time))

for j in range(5):
	print(res_arr[-5+j], end='\t')
print()

ctmc_model["place_list"].append("safe")
for i in range(no_places+1):
	#print("added plot for state:{}".format(i))
	plt.plot(res_arr[i*no_steps:(i+1)*no_steps], label = str(ctmc_model["place_list"][i]))
	#plt.plot(res_arr[i*no_steps:(i+1)*no_steps])
	
'''plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),
	ncol=3, fancybox=True, shadow=True)'''
plt.grid()

plt.savefig(tree_name + "_state_probabilities_f8.png")
plt.clf()
'''for j in range(no_states):
	print(res_arr[j*no_steps:(j+1)*no_steps])
	print()'''

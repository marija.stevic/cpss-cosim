#!/usr/bin/env python
# coding: utf-8

# In[1]:


# In[2]:


import numpy as np
import pandas as pd
import json
from pandas import ExcelWriter
from pandas import ExcelFile


# In[4]:


file = 'defender-at_ELES_trial_T-scenario-2020-3_11-0_cosim_ver0.xlsx'
sheet='T attack tree'

df = pd.read_excel(file, sheet_name=sheet)

print("Column headings:")
print(df.columns)


# In[5]:


# Create JSON object
model_json={
    "attackModel": [
    ]
}


# In[6]:


# Add attack tree object structure
add_attack_tree_str={
    "attackTree_ID": None,
    "root": {
        "node_ID": None,
        "node_name": None
    },
    "subgoal_nodes": [
    ],
    "leaf_nodes": [
    ],
    "edges": [
    ],
    "transitions": [
    ]
}


# In[7]:


model_json["attackModel"].append(add_attack_tree_str) 


# In[8]:


str_node=df['Name'][0]
str_ID=str_node[0:str_node.index("_")]
model_json["attackModel"][0]["attackTree_ID"]=str_ID

for i in df.index:
    if df['Root'][i]==1:
        model_json["attackModel"][0]["root"]["node_ID"]=df['Name'][i]
        model_json["attackModel"][0]["root"]["node_name"]=df['Description'][i]


# In[9]:


# Add Subgoal Nodes
subgoal_nodes=[
]

for i in df.index:
    if df['Subgoals'][i]==1:
        subgoal_node_i={
            "node_ID": df['Name'][i],
            "goal": df['Description'][i]
        }
        subgoal_nodes.append(subgoal_node_i)

model_json["attackModel"][0]["subgoal_nodes"]=subgoal_nodes


# In[10]:


# Add Leaf Nodes
leaf_nodes=[
]

for i in df.index:
    if df['Leaf'][i]==1:
        leaf_nodes_i={
            "node_ID": df['Name'][i],
            "goal": df['Description'][i],
            "probability_leaf_node": df['Probability'][i]
        }
        leaf_nodes.append(leaf_nodes_i)

model_json["attackModel"][0]["leaf_nodes"]=leaf_nodes


# In[11]:


# Add Edges
edges=[
]

for i in df.index:
    if df['OR'][i]==1:
        parent = df['Parents'][i]
        #print(parent)
        parent_list=parent.split(", ")
        parent_list_length=len(parent_list)
        #print(parent_list_length)
        #print(parent_list)
        j = 0
        for trans_j in parent_list:
            #print(trans_j)
            j=j+1
            edge_j={
            "edge_ID": "t"+str(j)+"_1_"+df['Name'][i],
            "parent": parent_list[j-1],
            "child": df['Name'][i],
            "type": "or"
            }
            edges.append(edge_j)
    if df['OR'][i]==0:
        parent = df['Parents'][i]
        #print(parent)
        parent_list=parent.split(", ")
        parent_list_length=len(parent_list)
        #print(parent_list_length)
        #print(parent_list)
        j = 0
        for trans_j in parent_list:
            #print(trans_j)
            j=j+1
            edge_j={
            "edge_ID": "t1_"+str(j)+"_"+df['Name'][i],
            "parent": parent_list[j-1],
            "child": df['Name'][i],
            "type": "and"
            }
            edges.append(edge_j)


model_json["attackModel"][0]["edges"]=edges


# In[19]:


# Add Transitions
transitions=[
]

for i in df.index:
    if df['OR'][i]==1:
        input_places = df['Parents'][i]
        input_places_list=input_places.split(", ")
        compromise_time=str(df['Compromise time'][i])
        compromise_time_list=compromise_time.split(", ")
        j = 0
        for trans_j in input_places_list:
            j=j+1
            trans_j={
            "t_ID": "t"+str(j)+"_"+df['Name'][i],
            "inputPlaces": [input_places_list[j-1]],
            "outputPlaces": [df['Name'][i]],
            "compromise_t": int(compromise_time_list[j-1])
            }
            transitions.append(trans_j)
    
    if df['OR'][i]==0:
        input_places = df['Parents'][i]
        input_places_list=input_places.split(", ")
        trans_j={
            "t_ID": "t1"+"_"+df['Name'][i],
            "inputPlaces": input_places_list,
            "outputPlaces": [df['Name'][i]],
            "compromise_t": int(df['Compromise time'][i])
            }
        transitions.append(trans_j)

model_json["attackModel"][0]["transitions"]=transitions


# In[20]:


with open('AttackTree_T.json', 'w') as outfile:
    json.dump(model_json, outfile, indent="\t")


# In[ ]:





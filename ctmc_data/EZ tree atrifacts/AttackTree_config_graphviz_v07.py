#!/usr/bin/env python
# coding: utf-8

# # Attack Tree generator for pn-cpss-cosim
# 

# In[1]:


pip install pandas


# In[27]:


pip install xlrd


# In[28]:


pip install graphviz


# In[1]:


import numpy as np
import pandas as pd
import json
from pandas import ExcelWriter
from pandas import ExcelFile
from graphviz import Digraph
import functools
from more_itertools import locate
import snakes.plugins
snakes.plugins.load("gv", "snakes.nets", "nets")
from nets import *


# In[2]:


file = 'EZ.xlsx'
sheet='attack_tree'
Attacktree_ID="EZ"
df = pd.read_excel(file, sheet_name=sheet)

print("Column headings:")
print(df.columns)


# In[3]:


for i in df.index:
    print(df['Name'][i])


# In[4]:


# Create JSON object
model_json={
    "attackModel": [
    ]
}


# In[5]:


# Add attack tree object structure
add_attack_tree_str={
    "attackTree_ID": None,
    "root": {
        "node_ID": None,
        "node_name": None
    },
    "subgoal_nodes": [
    ],
    "leaf_nodes": [
    ],
    "edges": [
    ],
    "transitions": [
    ]
}


# In[6]:


model_json["attackModel"].append(add_attack_tree_str)


# In[7]:


# Add ID and Root Node

str_node=df['Name'][0]
str_ID=str_node[0:str_node.index("_")]
model_json["attackModel"][0]["attackTree_ID"]=str_ID

for i in df.index:
    if df['Root'][i]==1:
        model_json["attackModel"][0]["root"]["node_ID"]=df['Name'][i]
        model_json["attackModel"][0]["root"]["node_name"]=df['Description'][i]


# In[8]:


# Add Subgoal Nodes
subgoal_nodes=[
]

for i in df.index:
    if df['Subgoals'][i]==1:
        subgoal_node_i={
            "node_ID": df['Name'][i],
            "goal": df['Description'][i]
        }
        subgoal_nodes.append(subgoal_node_i)

model_json["attackModel"][0]["subgoal_nodes"]=subgoal_nodes


# In[9]:


# Add Leaf Nodes
leaf_nodes=[
]

for i in df.index:
    if df['Leaf'][i]==1:
        leaf_nodes_i={
            "node_ID": df['Name'][i],
            "goal": df['Description'][i],
            "probability_leaf_node": df['Probability'][i]
        }
        leaf_nodes.append(leaf_nodes_i)

model_json["attackModel"][0]["leaf_nodes"]=leaf_nodes


# In[10]:


# Add Edges
edges=[
]

for i in df.index:
    if df['OR'][i]==1:
        parent = df['Parents'][i]
        #print(parent)
        parent_list=parent.split(", ")
        parent_list_length=len(parent_list)
        #print(parent_list_length)
        #print(parent_list)
        j = 0
        for trans_j in parent_list:
            #print(trans_j)
            j=j+1
            edge_j={
            "edge_ID": "t"+str(j)+"_1_"+df['Name'][i],
            "parent": parent_list[j-1],
            "child": df['Name'][i],
            "type": "or"
            }
            edges.append(edge_j)
    if df['OR'][i]==0:
        parent = df['Parents'][i]
        #print(parent)
        parent_list=parent.split(", ")
        parent_list_length=len(parent_list)
        #print(parent_list_length)
        #print(parent_list)
        j = 0
        for trans_j in parent_list:
            #print(trans_j)
            j=j+1
            edge_j={
            "edge_ID": "t1_"+str(j)+"_"+df['Name'][i],
            "parent": parent_list[j-1],
            "child": df['Name'][i],
            "type": "and"
            }
            edges.append(edge_j)


model_json["attackModel"][0]["edges"]=edges


# In[11]:


# Add Transitions
transitions=[
]

for i in df.index:
    if df['OR'][i]==1:
        input_places = df['Parents'][i]
        input_places_list=input_places.split(", ")
        compromise_time=str(df['Compromise time'][i])
        compromise_time_list=compromise_time.split(", ")
        j = 0
        for trans_j in input_places_list:
            j=j+1
            trans_j={
            "t_ID": "t"+str(j)+"_"+df['Name'][i],
            "inputPlaces": [input_places_list[j-1]],
            "outputPlaces": [df['Name'][i]],
            "compromise_t": int(compromise_time_list[j-1])
            }
            transitions.append(trans_j)
    
    if df['OR'][i]==0:
        input_places = df['Parents'][i]
        input_places_list=input_places.split(", ")
        trans_j={
            "t_ID": "t1"+"_"+df['Name'][i],
            "inputPlaces": input_places_list,
            "outputPlaces": [df['Name'][i]],
            "compromise_t": int(df['Compromise time'][i])
            }
        transitions.append(trans_j)

model_json["attackModel"][0]["transitions"]=transitions


# In[12]:


attacktree_json='AttackTree_'+Attacktree_ID+'.json'
with open(attacktree_json, 'w') as outfile:
    json.dump(model_json, outfile, indent="\t")


# In[13]:


# Use graphviz to draw attack tree
def draw_attack_model(attackModel):
    for attackTree_i in attackModel["attackModel"]:
        # create a graph 
        ap_graph_i = Digraph(name=attackTree_i["attackTree_ID"],
                             node_attr={'shape': 'octagon', 'style': 'filled', 'fillcolor': 'lightblue1'})
        # add leaf nodes
        for leaf_node_i in attackTree_i["leaf_nodes"]:
            ap_graph_i.node(leaf_node_i["node_ID"],
                            leaf_node_i["node_ID"] + '\n' + 'p = %s' % leaf_node_i["probability_leaf_node"],
                            margin='0.25')

        # add subgoal nodes
        for subgoal_node_i in attackTree_i["subgoal_nodes"]:
            ap_graph_i.node(subgoal_node_i["node_ID"], margin='0.25')

        # add root node
        ap_graph_i.node(attackTree_i["root"]["node_ID"], margin='0.25')

        # add edges
        for edge_i in attackTree_i["edges"]:
            ap_graph_i.edge(edge_i["parent"], edge_i["child"], edge_i["type"])

        #ap_graph_i.edge_attr['style'] = 'dashed'

        # draw graph
        ap_graph_i.format = 'png'
        ap_graph_i.render('AttackTree_Figure', view=False)

    return None

draw_attack_model(model_json)


# In[14]:


#place order in marking labels
place_list=None

#prefix notation
place_tree_rep=None

aggregated_label_cache=dict()

def get_place_label(marking):
    res=[]
    for p in place_list:
        if(p in marking):
            res.append(1)
            #res.append(len(marking(p)))
        else:
            res.append(0)
    return res

def get_all_indexes(dfs_tree):
    if len(dfs_tree)==1:
        return [place_list.index(dfs_tree[0])]
    #print(dfs_tree)
    indexes=list()
    for i in range(len(dfs_tree)):
        if not isinstance(dfs_tree[i], list):
            indexes.extend([place_list.index(dfs_tree[i])])
        else:
            indexes.extend(get_all_indexes(dfs_tree[i]))
    return indexes

def get_descendent_indexes(sub_tree_root_name, dfs_tree):
    #print(sub_tree_root_name, dfs_tree)
    if dfs_tree[0]==sub_tree_root_name:
        return get_all_indexes(dfs_tree)
    else:
        for i in range(1, len(dfs_tree)):
           indexes=get_descendent_indexes(sub_tree_root_name, dfs_tree[i])
           if indexes!=None:
                return indexes
    return None


def get_aggregated_label(label):
    if str(label) in aggregated_label_cache:
        return aggregated_label_cache[str(label)]
    #print(label)
    res=['0']*len(label)
    for i in range(len(label)):
        if label[i]==1 and res[i]=='0':
            descendent_indexes=get_descendent_indexes(place_list[i], place_tree_rep)
            #print(descendent_indexes)
            for j in descendent_indexes:
                res[j]='*'
            res[i]='1'

    aggregated_label_cache[str(label)]=res
    #print("label: %s, aggr_label: %s" % (str(label), str(res)))
    return res


def is_multi_token_state(marking):
    for p in place_list:
        if(p in marking and len(marking[p])>1):
            return True
    return False

#transition_lists: list of list of tuples, tuple=>(state_to, transition_name)
#names: original, without multitoken states, aggregated states
def draw_state_graph(state_labels, transition_lists, name_of_graph, multi_token_states=None):

    state_graph = Digraph(name=name_of_graph,
                             node_attr={'shape': 'rect', 'style': 'filled', 'fillcolor': 'lightblue1'})

    i = 0
    for  l in state_labels:
        if multi_token_states and i in multi_token_states:
            i+=1
            continue
        state_graph.node(str(i), str(l))
        i=i+1

    i = 0
    for trans_list in transition_lists:
        for trans in trans_list:
            #print("adding edge from %s to %s with label %s" % (str(i), str(trans[0]), trans[1]))
            state_graph.edge(str(i), str(trans[0]), label=trans[1])
        i = i + 1

    state_graph.format = 'png'
    state_graph.render("state_graph_" + name_of_graph)
    return

def dfs_find_add(parent, child, prefix_notation):
    if prefix_notation[0]!=parent:
        for i in range(1,len(prefix_notation)):
            dfs_find_add(parent, child, prefix_notation[i])
    else:
        prefix_notation.append([child])

def get_parent(edge_list, child):

    for i in edge_list:
        if i["parent"]==child:
            return i["child"]
    
    return ""


def get_prefix_notation(attack_tree):

    prefix_notation=list()
    trans_list=list()
    
    prefix_notation.append(attack_tree["root"]["node_ID"])

    for i in attack_tree["subgoal_nodes"]:
        dfs_find_add(get_parent(attack_tree["edges"], i["node_ID"]), i["node_ID"], prefix_notation)

    for i in attack_tree["leaf_nodes"]:
        dfs_find_add(get_parent(attack_tree["edges"], i["node_ID"]), i["node_ID"], prefix_notation)

    for i in attack_tree["transitions"]:
        trans_list.append(i["t_ID"])

    return [prefix_notation, trans_list]
    
def get_CTMC_repr(place_list, aggregate_state_labels, aggregated_states_transitions):

    ctmc_obj=dict()
    ctmc_obj["place_list"]=place_list
    ctmc_obj["aggregate_state_labels"]=list()
    for i in aggregate_state_labels:
        ctmc_obj["aggregate_state_labels"].append(str(i))

    ctmc_obj["aggregated_states_transitions"]=list()
    for i in aggregated_states_transitions:
        #TODO ask about self loops
        trans=dict()
        trans["from"]=i[0]
        trans["to"]=i[1]
        trans["t_list"]=aggregated_states_transitions[i]
        ctmc_obj["aggregated_states_transitions"].append(trans)

    return ctmc_obj

def get_CTMC_compressed_repr(place_list, trans_list, aggregate_state_labels, aggregated_states_transitions):

    ctmc_obj=dict()
    ctmc_obj["place_list"]=place_list
    ctmc_obj["aggregate_state_labels"]=list()
    for i in aggregate_state_labels:
        ctmc_obj["aggregate_state_labels"].append(''.join(map(str, i)))

    ctmc_obj["aggregated_states_transitions"]=dict()
    #list of tuples
    ctmc_obj["aggregated_states_transitions"]["indexes"]=list()
    ctmc_obj["aggregated_states_transitions"]["lists"]=list()
    for i in aggregated_states_transitions:
        #TODO ask about self loops
        ctmc_obj["aggregated_states_transitions"]["indexes"].append(i)
        t_list=list()
        for j in aggregated_states_transitions[i]:
            t_list.append(trans_list.index(j))
        ctmc_obj["aggregated_states_transitions"]["lists"].append(t_list)

    return ctmc_obj


def create_pn(attack_tree):

    pn=PetriNet("attack tree EZ")
    
    pn.add_place(Place(attack_tree["root"]["node_ID"]))

    for s_goal in attack_tree["subgoal_nodes"]:
        pn.add_place(Place(s_goal["node_ID"]))

    for leaf in attack_tree["leaf_nodes"]:
        pn.add_place(Place(leaf["node_ID"], [dot]))

    for trans in attack_tree["transitions"]:
        pn.add_transition(Transition(trans["t_ID"]))
        for i in trans["inputPlaces"]:
            pn.add_input(i, trans["t_ID"], Variable("x"))
        for i in trans["outputPlaces"]:
            pn.add_output(i, trans["t_ID"], Variable("x"))

    pn.draw("petri_net_"+Attacktree_ID+".png")

    return pn

def compare_node_names(node1, node2):
    if(len(node1)>len(node2)):
        return 1
    elif(node1 > node2):
        return 1
    else:
        return -1

def place_list_repr(attack_tree):
    list_repr=list()
    subgoals=list()
    leaves=list()

    list_repr.append(attack_tree["root"]["node_ID"])

    for s_goal in attack_tree["subgoal_nodes"]:
        subgoals.append(s_goal["node_ID"])
    subgoals.sort(key=functools.cmp_to_key(compare_node_names))

    for leaf in attack_tree["leaf_nodes"]:
        leaves.append(leaf["node_ID"])
    leaves.sort(key=functools.cmp_to_key(compare_node_names))

    #print("%r \n %r\n" % (subgoals, leaves))

    list_repr.extend(subgoals)
    list_repr.extend(leaves)
    list_repr.reverse()

    return list_repr


# In[15]:


trans_list=None
pn=None
with open('AttackTree_'+Attacktree_ID+'.json') as json_file:
    atree=json.load(json_file)
    [place_tree_rep,trans_list]=get_prefix_notation(atree["attackModel"][0])
    print("tree prefix notation: %r" % (place_tree_rep))

    pn=create_pn(atree["attackModel"][0])
    place_list=place_list_repr(atree["attackModel"][0])
    print("place order in label: %r" % (place_list))

g = StateGraph(pn)
labels = list()
multi_token_states = list()
absorbing_states = list()
for state in g :
    m = g.net.get_marking()
    label = get_place_label(m)
    labels.append((label))
    if(is_multi_token_state(m)):
        multi_token_states.append(state)
    #print('state %s label %s' % (state, label))

print("======================================================")
transition_lists = list()
no_ctmc_states = 0

# keeps track if state is reachable
# intended to remove states reachable only from multi-token-states
transition_enabled = np.zeros(len(labels))
# mark initial state
transition_enabled[0] = 1

aggregated_states=list()
aggregated_states.append(get_aggregated_label(labels[0]))
#key tuple: (from, to) in index aggregated_states, value: list of transition name(should be unique)
aggregated_states_transitions=dict()
#contains same info as dict, used for plotting
aggregated_states_transitions_list=list()

simple_states=list()
simple_states.append(labels[0])
simple_states_transitions=dict()

transition_list=list()
state_index=-1

for state in g :

    state_index+=1
    # skip all multi_token_states and the states only reachable through them
    # this holds true as stategraph is calculated incrementally
    if(state in multi_token_states or transition_enabled[state]==0):
        transition_lists.append([])
        continue

    g.goto(state)
    no_ctmc_states += 1

    successors=list(g.successors())
    transition_list.clear()

    # get aggregated state index for current state(already added)
    cur_agg_index=aggregated_states.index(get_aggregated_label(labels[state_index]))
    cur_simp_index=simple_states.index(labels[state_index])
    for s in successors:
        if s[0] not in multi_token_states:
            transition_list.append([s[0], s[1].name])
            transition_enabled[s[0]]=1
            # get aggregated state notation/index for successor state
            succ_agg_label=get_aggregated_label(labels[s[0]])
            
            if labels[s[0]] not in simple_states:
                simple_states.append(labels[s[0]])
            index_succ_simp_label=simple_states.index(labels[s[0]])
            simple_states_transitions[(cur_simp_index, index_succ_simp_label)] = [s[1].name]
            
            index_succ_agg_label=-1
            if succ_agg_label not in  aggregated_states:
                aggregated_states.append(succ_agg_label)
                index_succ_agg_label=len(aggregated_states)-1
            else:
                index_succ_agg_label=aggregated_states.index(succ_agg_label)
                
                
            # get list in aggregated_states_transitions,
            # (cur_index, succ_index) ? create, append : unique check(not in) append
            if (cur_agg_index, index_succ_agg_label) not in aggregated_states_transitions:
                aggregated_states_transitions[(cur_agg_index, index_succ_agg_label)] =                     [s[1].name]
            elif s[1].name not in aggregated_states_transitions[(cur_agg_index, index_succ_agg_label)]:
                aggregated_states_transitions[(cur_agg_index, index_succ_agg_label)].append(s[1].name)
            
            # TODO may combine leaf probabilities here too
    transition_lists.append(list(transition_list))

    # review logic of detecting absorbing states via no outgoing transitions
    if(len(transition_list)==0):
        absorbing_states.append(state)

    #print('state %s successors: %s, to states %s' % (state,str(transitions), str(transitioned_states)))


draw_state_graph(labels, transition_lists, "without _multitoken_states", multi_token_states)

print("aggregated labels:")
for i in aggregated_states:
    print(i)
    aggregated_states_transitions_list.append(list())

print("aggregated state transitions:")
for i in aggregated_states_transitions:
    print("%r: %r"%(i, aggregated_states_transitions[i]))
    aggregated_states_transitions_list[i[0]].append([i[1], str(aggregated_states_transitions[i])])

print(aggregated_states_transitions_list)
draw_state_graph(aggregated_states, aggregated_states_transitions_list, "_aggregated_states")

print("aggregated labels mapping:")
aggregated_label_mapping=dict()
j=0
for i in aggregated_label_cache:
    print("%r : %r " % (i, aggregated_label_cache[i]))
    if str(aggregated_label_cache[i]) not in aggregated_label_mapping:
        aggregated_label_mapping[str(aggregated_label_cache[i])]=list()
    aggregated_label_mapping[str(aggregated_label_cache[i])].append(j)
    j+=1

print(len(aggregated_states))
for i in aggregated_label_mapping:
    print("%r : %r " % (i, len(aggregated_label_mapping[i])))

ctmc_obj=get_CTMC_repr(place_list, aggregated_states, aggregated_states_transitions)
with open('ctmc_'+Attacktree_ID+'.json', 'w') as outfile:
    json.dump(ctmc_obj, outfile, indent=4)
          
ctmc_simple_obj=get_CTMC_repr(place_list, simple_states, simple_states_transitions)
ctmc_simple_obj["aggregated_mapping"]=aggregated_label_mapping
with open('ctmc_'+Attacktree_ID+'_simple.json', 'w') as outfile:
    json.dump(ctmc_simple_obj, outfile, indent=4)

print(len(aggregated_states_transitions))
print(len(simple_states_transitions))
ctmc_comp_obj=get_CTMC_compressed_repr(place_list, trans_list, aggregated_states, aggregated_states_transitions)
with open('ctmc_' + Attacktree_ID + '_p.json', 'w') as outfile:
    json.dump(ctmc_comp_obj, outfile)
    
ctmc_comp_obj=get_CTMC_compressed_repr(place_list, trans_list, simple_states, simple_states_transitions)
ctmc_comp_obj["aggregated_mapping"]=aggregated_label_mapping
with open('ctmc_' + Attacktree_ID + '_simple_p.json', 'w') as outfile:
    json.dump(ctmc_comp_obj, outfile)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





import numpy as np
from scipy.integrate import ode
import matplotlib.pyplot as plt
import random
import time
import json

def get_leaf_probability(node_name, leaf_nodes):
    for leaf in leaf_nodes:
        if leaf["node_ID"]==node_name:
            return leaf["probability_leaf_node"]
    # this line is fail-safe only
    return 1.0

def generate_state_matrix(max_states, ctmc_json, at_tree, step_size_dt):
    nodes=ctmc_json["place_list"]
    state_transition_matrix=dict()
    for i in range(len(ctmc_json["aggregated_states_transitions"]["indexes"])):
        key=tuple(ctmc_json["aggregated_states_transitions"]["indexes"][i])
        if(key[0]==key[1]):
            continue
        elif key not in state_transition_matrix:
            state_transition_matrix[key]=0.0            
        val=ctmc_json["aggregated_states_transitions"]["lists"][i]
        for j in range(len(val)):
            trans_j=at_tree["transitions"][val[j]]
            trans_Rate=step_size_dt/trans_j["compromise_t"]
            for l in trans_j["inputPlaces"]:
                trans_Rate*=get_leaf_probability(l, at_tree["leaf_nodes"])
            state_transition_matrix[key]+=trans_Rate
    return state_transition_matrix

    
def infitismal_generator(state_transition_matrix, no_states):

    Q = np.zeros((no_states, no_states))

    for vec in state_transition_matrix.keys():
        Q[vec[0], vec[1]] += state_transition_matrix[vec]
    
    for i in range(no_states):
        #Q[i,i] is also being summed up
        Q[i,i] = 2*Q[i,i] - np.sum(Q[i, :])
    
    return Q

def steady_state_calculator(no_states, Q):

    b = np.zeros(no_states+1,1)
    QQ = np.ones(1, no_states)

    A = np.concatenate(np.transpose(Q), QQ)

    A_transposed = np.transpose(A)
    intermediate_res = np.matmul(A_transposed, A)
    intermediate_res = np.linalg.solve(intermediate_res, np.eye(len(intermediate_res)))
    intermediate_res = np.matmul(intermediate_res, A_transposed)
    steady_st_prob = np.matmul(intermediate_res, b)

    return steady_st_prob
    
def detect_absorption_states(Q):

    print(Q)
    print("-------------------------------")
    abs_state=list()
    for i in range(len(Q)):
        if np.count_nonzero(Q[i])==0:
            abs_state.append(i)
    print("abs_state:")
    print(abs_state)
    return abs_state

def reorder_states(Q, abs_state):
    tail=len(Q)-1
    tails=list()
    for a in abs_state:    
        tails.append(tail)
        tail -= 1
    print(tails)
    state_order=list(range(len(Q)))
    for (a,t) in zip(abs_state,tails):
        state_order[a]=t
        state_order[t]=a
        temp = Q[a].copy()
        Q[a] = Q[t].copy() 
        Q[t] = temp
        temp = Q[:,a].copy()
        Q[:,a] = Q[:,t].copy()
        Q[:,t] = temp
    print(Q)
    print("-------------------------------")
    return state_order

def diff_eqns(t, pi_t, Q):

    no_states = len(Q)
    dpidt = np.zeros(no_states)
   
    for i in range(no_states): 
        for j in range(no_states):
            dpidt[i] += pi_t[j]*Q[j,i]
        
    return dpidt

def state_occupancy_time(Q, pi_0, no_states, no_absorbing_states):

    time_to_absorption = np.zeros(no_absorbing_states)                                                                     
    pi_abs_infinity = np.zeros(no_absorbing_states)                                                                        
    time_to_absorption_numerator = np.zeros(no_absorbing_states)                                                           
    
    # Generating matrices required for calculating time_to_absorption 
    # and time_present_in_transient_states
    num_transient_states = no_states - no_absorbing_states               
    Q_u = Q[0:num_transient_states, 0:num_transient_states]      
    pi_0_u = pi_0[0:num_transient_states] 
    inv_Q_u = -1 * np.linalg.solve(Q_u, np.eye(len(Q_u)))     
    e = np.ones(no_states-1)

    # Calculation of time_present_in_transient_states before absorption
    # mean time spent in transient state
    time_present_in_transient_states = np.matmul(pi_0_u, inv_Q_u)

    if no_absorbing_states!=1 :   
        for i in range(no_absorbing_states):
            # Calculate the final probability of absorption to 'i'th absorption state 
            pi_abs_infinity[i] = np.matmul(time_present_in_transient_states,\
                 Q[0:num_transient_states, num_transient_states+i])

        # Calculation of time_to_absorption
        # TODO: review translates to (pi_0_u * inv_Q_u * inv_Q_u * Q(part))
        intermediate_res = np.matmul(time_present_in_transient_states, inv_Q_u)

        for i in range(no_absorbing_states):
            # Calculate the numerator of time to absorption to 'i'th absorbing state
            time_to_absorption_numerator[i] = np.matmul(intermediate_res, \
                Q[0:num_transient_states, num_transient_states+i])
            # Mean time to absorbing in 'i'th absorbing state
            time_to_absorption[i] = time_to_absorption_numerator[i]/pi_abs_infinity[i]
    else:
        # Mean time to absorption
        time_to_absorption = np.matmul(time_present_in_transient_states, np.transpose(e))

    return (time_to_absorption, time_present_in_transient_states)        

def script_main(input):    
    step_size_dt=input["step_size_dt"]
    t1=input["time_horizon"]
    max_step_integ=input["max_step"]
    dt=input["step_size_integrator"]

    ctmc_json=None
    at_tree=None
    with open("ctmc_EZ_simple_p.json") as file:
        ctmc_json = json.load(file)
    with open("AttackTree_EZ.json") as file:
        at_tree = json.load(file)
        
    no_states=len(ctmc_json["aggregate_state_labels"])
    state_transition_matrix=generate_state_matrix(no_states, ctmc_json, at_tree["attackModel"][0], step_size_dt)
    print(state_transition_matrix)
    start_time = time.time()

    Q = infitismal_generator(state_transition_matrix, no_states)
    abs_states = detect_absorption_states(Q)
    state_order = reorder_states(Q, abs_states)

    pi_0=np.zeros(no_states)
    pi_0[0]=1
    print("pi_0")
    print(pi_0)

    # (0.823509823509824    0.436229667176549), 
    # (0.333333333333333    0.0606060606060606    0.187590187590188)
    # (time_to_absorption, time_present_in_transient_states) = \
    #     state_occupancy_time(Q, pi_0, no_states, len(abs_states))

    # print(str(time_to_absorption))
    # print(time_present_in_transient_states)
    # print("--- %s seconds ---" % (time.time() - start_time))

    r = ode(diff_eqns).set_integrator('dopri5', atol=1e-3, max_step=max_step_integ)
    r.set_initial_value(pi_0, 0).set_f_params(Q)
    sol=np.zeros((int(t1/dt),no_states))
    i=0
    print(r.t)
    while r.successful() and r.t < t1:
        sol[i] = r.integrate(r.t+dt)
        i += 1

    print(len(sol))
    print(sol[-1])
    tot_time=time.time() - start_time
    print("--- %s seconds ---" % (tot_time))


    sol_trans = np.transpose(sol)
    plt.figure()
    for i in range(len(sol_trans)):
        plt.plot(sol_trans[i])

    plt.grid(which='both')
    #plt.minorticks_on()
    plt.savefig("simp_plt_" + str(input["step_size_dt"]) + "_" + str(input["step_size_integrator"]) + "_" + str(input["time_horizon"]) + "_" + str(input["max_step"]) + ".jpg")
    plt.close()

    aggr_sol=np.zeros((len(ctmc_json["aggregated_mapping"]),int(t1/dt)))
    i=0
    for ags in ctmc_json["aggregated_mapping"]:
        for s in ctmc_json["aggregated_mapping"][ags]:
            np.add(aggr_sol[i], sol_trans[s], out=aggr_sol[i])
        i+=1
    
    plt.figure()
    for i in range(len(aggr_sol)):
        plt.plot(aggr_sol[i])

    plt.grid(which='both')
    #plt.minorticks_on()
    plt.savefig("simp_aggr_plt_" + str(input["step_size_dt"]) + "_" + str(input["step_size_integrator"]) + "_" + str(input["time_horizon"]) + "_" + str(input["max_step"]) + ".jpg")
    plt.close()

    node_sol=np.zeros((len(ctmc_json["place_list"]),int(t1/dt)))
    i=0
    for s in range(len(sol_trans)):
        for n in range(len(ctmc_json["place_list"])):
            if(ctmc_json["aggregate_state_labels"][state_order[s]][n]=='1'):
                node_sol[n] = np.add(node_sol[n], sol_trans[s])
    
    plt.figure()
    for i in range(len(node_sol)):
        plt.plot(node_sol[i], label=ctmc_json["place_list"][i])

    plt.grid(which='both')
    plt.legend()
    #plt.minorticks_on()
    plt.savefig("simp_node_plt_" + str(input["step_size_dt"]) + "_" + str(input["step_size_integrator"]) + "_" + str(input["time_horizon"]) + "_" + str(input["max_step"]) + ".jpg")
    plt.close()
    
    return tot_time

if __name__=='__main__':
    np.printoptions(precision=3, threshold=np.inf)
    input=dict()
    input["step_size_dt"]=1
    input["step_size_integrator"]=1
    input["time_horizon"]=70
    input["max_step"]=0.1
    script_main(input)
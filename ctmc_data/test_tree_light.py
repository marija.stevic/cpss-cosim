import numpy as np
import json
import functools

from graphviz import Digraph
from more_itertools import locate

import time

#place order in marking labels
place_list=None

#prefix notation
place_tree_rep=None

aggregated_label_cache=dict()

def get_all_indexes(dfs_tree):
    if len(dfs_tree)==1:
        return [place_list.index(dfs_tree[0])]
    #print(dfs_tree)
    indexes=list()
    for i in range(len(dfs_tree)):
        if not isinstance(dfs_tree[i], list):
            indexes.extend([place_list.index(dfs_tree[i])])
        else:
            indexes.extend(get_all_indexes(dfs_tree[i]))
    return indexes

def get_descendent_indexes(sub_tree_root_name, dfs_tree):
    #print(sub_tree_root_name, dfs_tree)
    if dfs_tree[0]==sub_tree_root_name:
        return get_all_indexes(dfs_tree)
    else:
        for i in range(1, len(dfs_tree)):
           indexes=get_descendent_indexes(sub_tree_root_name, dfs_tree[i])
           if indexes!=None:
                return indexes
    return None    

def get_aggregated_label(label):
    if str(label) in aggregated_label_cache:
        return aggregated_label_cache[str(label)]
    #print(label)
    res=['0']*len(label)
    for i in range(len(label)):
        if label[i]=='1' and res[i]=='0':
            descendent_indexes=get_descendent_indexes(place_list[i], place_tree_rep)
            #print(descendent_indexes)
            for j in descendent_indexes:
                res[j]='*'
            res[i]='1'

    aggregated_label_cache[str(label)]=res
    #print("label: %s, aggr_label: %s" % (str(label), str(res)))
    return res

#transition_lists: list of list of tuples, tuple=>(state_to, transition_name)
#names: original, without multitoken states, aggregated states
def draw_state_graph(state_labels, transition_lists, name_of_graph):

    state_graph = Digraph(name=name_of_graph,
                             node_attr={'shape': 'rect', 'style': 'filled', 'fillcolor': 'lightblue1'})

    i = 0
    for  l in state_labels:
        state_graph.node(str(i), str(l))
        i=i+1

    i = 0
    for trans_list in transition_lists:
        for trans in trans_list:
            #print("adding edge from %s to %s with label %s" % (str(i), str(trans[0]), trans[1]))
            state_graph.edge(str(i), str(trans[0]), label=trans[1])
        i = i + 1

    state_graph.format = 'png'
    state_graph.render("state_graph_" + name_of_graph)
    return

def dfs_find_add(parent, child, prefix_notation):
    if prefix_notation[0]!=parent:
        for i in range(1,len(prefix_notation)):
            dfs_find_add(parent, child, prefix_notation[i])
    else:
        prefix_notation.append([child])

def get_parent(edge_list, child):

    for i in edge_list:
        if i["parent"]==child:
            return i["child"]
    
    return ""


def get_prefix_notation(attack_tree):

    prefix_notation=list()
    trans_list=list()
    
    prefix_notation.append(attack_tree["root"]["node_ID"])

    for i in attack_tree["subgoal_nodes"]:
        dfs_find_add(get_parent(attack_tree["edges"], i["node_ID"]), i["node_ID"], prefix_notation)

    for i in attack_tree["leaf_nodes"]:
        dfs_find_add(get_parent(attack_tree["edges"], i["node_ID"]), i["node_ID"], prefix_notation)

    for i in attack_tree["transitions"]:
        trans_list.append(i["t_ID"])

    return [prefix_notation, trans_list]
    
def get_CTMC_repr(place_list, aggregate_state_labels, aggregated_states_transitions):

    ctmc_obj=dict()
    ctmc_obj["place_list"]=place_list
    ctmc_obj["aggregate_state_labels"]=list()
    for i in aggregate_state_labels:
        ctmc_obj["aggregate_state_labels"].append(str(i))

    ctmc_obj["aggregated_states_transitions"]=list()
    for i in aggregated_states_transitions:
        fr=int(i[1:i.index(',')])
        to=int(i[i.index(',')+1:-1])
        trans=dict()
        trans["from"]=fr
        trans["to"]=to
        trans["t_list"]=aggregated_states_transitions[i]
        ctmc_obj["aggregated_states_transitions"].append(trans)

    return ctmc_obj

def get_CTMC_compressed_repr(place_list, trans_list, aggregate_state_labels, aggregated_states_transitions):

    ctmc_obj=dict()
    ctmc_obj["place_list"]=place_list
    ctmc_obj["aggregate_state_labels"]=list()
    for i in aggregate_state_labels:
        ctmc_obj["aggregate_state_labels"].append("".join(i))

    ctmc_obj["aggregated_states_transitions"]=dict()
    #list of tuples
    ctmc_obj["aggregated_states_transitions"]["indexes"]=list()
    ctmc_obj["aggregated_states_transitions"]["lists"]=list()
    for i in aggregated_states_transitions:
        #TODO ask about self loops
        ctmc_obj["aggregated_states_transitions"]["indexes"].append(i)
        t_list=list()
        for j in aggregated_states_transitions[i]:
            t_list.append(trans_list.index(j))
        ctmc_obj["aggregated_states_transitions"]["lists"].append(t_list)

    return ctmc_obj

def compare_node_names(node1, node2):
    if(len(node1)>len(node2)):
        return 1
    elif(node1 > node2):
        return 1
    else:
        return -1

def place_list_repr(attack_tree):
    list_repr=list()
    subgoals=list()
    leaves=list()

    list_repr.append(attack_tree["root"]["node_ID"])

    for s_goal in attack_tree["subgoal_nodes"]:
        subgoals.append(s_goal["node_ID"])
    subgoals.sort(key=functools.cmp_to_key(compare_node_names))

    for leaf in attack_tree["leaf_nodes"]:
        leaves.append(leaf["node_ID"])
    leaves.sort(key=functools.cmp_to_key(compare_node_names))

    #print("%r \n %r\n" % (subgoals, leaves))

    list_repr.extend(subgoals)
    list_repr.extend(leaves)
    list_repr.reverse()

    return list_repr

start=time.time()
trans_list=None
pn=None
tree_name='G2'
with open('AttackTree_'+tree_name+'.json') as json_file:
    atree=json.load(json_file)
    atree=atree["attackModel"][0]
    [place_tree_rep,trans_list]=get_prefix_notation(atree)
    print("tree prefix notation: %r" % (place_tree_rep))

    place_list=place_list_repr(atree)
    print("place order in label: %r" % (place_list))

no_places=len(place_list)
print("======================================================")

#to optimize all possible transition from a place
#for each place (transition_id, [mandatory index], outgoing place index)
#as each place is used as input in only one transtion
#mandatory index is relevant only for AND transitions, includes current index too
#one entry if OR transtion
optimized_transition_entries=[()]*no_places
for trans in atree["transitions"]:
    transition_id=trans["t_ID"]
    mandatory_index=list()
    for i in trans["inputPlaces"]:
        mandatory_index.append(place_list.index(i))
    outgoing_place_index=place_list.index(trans["outputPlaces"][0])
    for i in mandatory_index:
        optimized_transition_entries[i]=\
            (transition_id, mandatory_index, outgoing_place_index)

print("optimized_transition_entries")
for i in optimized_transition_entries:
    print(i)

print("======================================================")
aggregated_states=list()
#key tuple: (from, to) in index aggregated_states, value: list of transition name(should be unique)
aggregated_states_transitions=dict()

#first state with token at all leaf_nodes
initial_state=['0']*no_places
for l in atree["leaf_nodes"]:
    initial_state[place_list.index(l["node_ID"])]='1'
aggregated_states.append(initial_state)

cur_state_index=-1
for s in aggregated_states:

    cur_state_index+=1
    # need to modify this entry but keep original intact
    cur_state=list(s)
    executed_transtion_index=list()
    for i in range(no_places):
        if i in executed_transtion_index:
            continue
        if cur_state[i]=='1':
            opt_tr=optimized_transition_entries[i]
            if(opt_tr==()):
                continue
            token_in_all_mandatory=True
            for j in opt_tr[1]:
                if cur_state[j]=='0':
                    token_in_all_mandatory=False
            #execute transition if token in all input places
            if token_in_all_mandatory:
                executed_transtion_index.extend(opt_tr[1])
                for j in opt_tr[1]:
                    cur_state[j]='*'
                cur_state[opt_tr[2]]='1'
                aggr_label=get_aggregated_label(cur_state)
                if aggr_label not in aggregated_states:
                    aggregated_states.append(aggr_label)
                    #print("appended aggregated state: %d, %r"%(cur_state_index, cur_state))
                key="("+str(cur_state_index)+","+str(aggregated_states.index(aggr_label))+")"
                cur_state[opt_tr[2]]='0'
                for j in opt_tr[1]:
                    cur_state[j]='1'
                #may use set
                if key not in aggregated_states_transitions:
                    aggregated_states_transitions[key]=list()
                #append transition id
                if opt_tr[0] not in aggregated_states_transitions[key]:
                    aggregated_states_transitions[key].append(opt_tr[0])
    

print("total aggregated states: %d" % len(aggregated_states))
'''print("aggregated labels:")
for i in aggregated_states:
    print(i)

print("aggregated state transitions:")
for i in aggregated_states_transitions:
    print("%r: %r"%(i, aggregated_states_transitions[i]))

#print(aggregated_states_transitions_list)
#draw_state_graph(aggregated_states, aggregated_states_transitions_list, "_aggregated_states")'''

ctmc_obj=get_CTMC_repr(place_list, aggregated_states, aggregated_states_transitions)
with open('ctmc_' + tree_name + '.json', 'w') as outfile:
    json.dump(ctmc_obj, outfile, indent=4)

ctmc_pickle_obj=get_CTMC_compressed_repr(place_list, trans_list, aggregated_states, aggregated_states_transitions)
#pickle.dump(ctmc_pickle_obj, open( "ctmc_R.p", "wb"))
with open('ctmc_' + tree_name + '_p.json', 'w') as outfile:
    json.dump(ctmc_pickle_obj, outfile)

print(time.time()-start)
print(len(aggregated_states))
print(len(aggregated_states_transitions))
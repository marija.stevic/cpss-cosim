#include <iostream>
#include <vector>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>      

#include <boost/numeric/odeint.hpp>
#include <array>
#include <chrono> 

using namespace std::chrono; 
using namespace std;
using namespace boost::numeric::odeint;

typedef std::vector< float > state_type;


class attack_tree_aggregated_solver {

    int no_states, num_trans;
    int *to_states, *from_states;
    float *trans_rate;

public:
    attack_tree_aggregated_solver(int no_states, int num_trans, int* to_states, int* from_states, float* trans_rate) {
        this->no_states = no_states;
        this->num_trans = num_trans;
        this->to_states = to_states;
        this->from_states = from_states;
        this->trans_rate = trans_rate;
     }

    void operator() ( const state_type &x , state_type &dxdt , const float /* t */ )
    {

        for(int i=0; i<no_states;i++)
            dxdt[i] = 0.0;

        for(int i=0; i<num_trans; i++){
                dxdt[to_states[i]] += x[from_states[i]]*trans_rate[i];
        }

    }
};


struct save_probability_time_series
{
    float *result;
    int index, steps, no_states, num_places;
    char** aggregated_labels;

    save_probability_time_series( float* result_t, int steps_t, int no_states_t, \
                    int num_places_t, char** aggregated_labels_t ){
        result=result_t;
        index=0;
        steps=steps_t;
        no_states=no_states_t;
        num_places=num_places_t;
        aggregated_labels=aggregated_labels_t;
    }

    void operator()( const state_type &x , double t )
    {   
        float sum_p=0.0;

        for(int s=0;s<no_states;s++)
            sum_p+=x[s];

        for(int p=0;p<num_places;p++)
            result[p*steps+index]=0.0;
        
        for(int s=0;s<no_states;s++){
            for(int p=0;p<num_places;p++){            
                if(aggregated_labels[s][p]!='0')
                    result[p*steps+index]+=x[s];
            }
        }

        for(int p=0;p<num_places;p++)
            result[p*steps+index]/=sum_p;
        
        index++;
    }
};



extern "C" void main_func(int no_states, int num_trans, int initial_state_index, 
    float final_time, int* to_states, int* from_states, float* individual_trans_rate, 
    int *transition_indexes, int *transition_indexes_len, int num_leaves, float *leaf_probabilities, float* result, 
    int num_places, char** aggregated_labels)
{

    srand (time(NULL));
    auto start = high_resolution_clock::now();

    float *trans_rate = new float[num_trans];
    for(int i=0; i<no_states; i++)
        trans_rate[num_trans-no_states+i]=0.0;

    for(int i=0, k=0; i<num_trans-no_states; i++){
        trans_rate[i]=0.0;
        for(int j=0; j<transition_indexes_len[i]; j++){
            trans_rate[i]+=individual_trans_rate[transition_indexes[k]];
            k++;
        }
        trans_rate[num_trans-no_states+from_states[i]]-=trans_rate[i];
    }

    state_type pi_0(no_states);
    for(int i=1; i<no_states;i++)
        pi_0[i] = 0.0;
    pi_0[initial_state_index] = 1.0;

    bulirsch_stoer_dense_out< state_type > stepper( 1E-9 , 1E-9 , 1.0 , 0.0 );


    double dt=1.0;
    int no_steps=(int) (final_time/dt);
    no_steps++;
    size_t steps = integrate_const(stepper, attack_tree_aggregated_solver(no_states, num_trans, to_states, from_states, trans_rate) ,
            pi_0 , 0.0 , (double) final_time , dt ,
            save_probability_time_series( result, no_steps, 
                no_states, num_places, aggregated_labels ) );

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start); 
  
    cout << "integrated in(us): " << duration.count() << endl;   

    /* output */

    float normalization_f;
    for(int p=0;p<num_leaves;p++){
        normalization_f=leaf_probabilities[p]/(result[p*no_steps]+0.001);
        for(int t=0; t<no_steps; t++)
            result[p*no_steps+t]*=normalization_f;
    }


    /*for(int i=0; i<no_states;i++)
    {   
        for( size_t t=0; t<no_steps; t++ )
            cout << result[i*no_steps+t] << '\t';
        cout<<"\n";
    }   */

    cout<<"results: "<<endl;
    for(int i=0; i<5;i++)
        cout << result[num_places*no_steps-5+i] << '\t';
    cout<<endl;

    delete []trans_rate;

    return;

}

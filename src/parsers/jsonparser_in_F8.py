import json

from snakes.data import MultiSet
import snakes.plugins
snakes.plugins.load("gv", "snakes.nets", "nets")
from nets import *
from copy import deepcopy
import logging
from parsers.ctmc_parser_utils import get_aggregated_label, get_place_label

from functools import reduce
import numpy as np
from api.perf_m import timer

import ctypes

# from snakes.nets import PetriNet, Place, Transition, Expression, Variable, MultiSet
# from snakes.nets import Substitution

logger=logging.getLogger('simulator_logger')

@timer
def parse_obj_f8(in_CEPinput, in_attackModel, ctmcModel, cmd_line=False):

	mitigation_model = None

	"""Add input-events to attackModel."""
	model = deepcopy(in_attackModel)    # copy the base model to keep it
	# parse events
	str_category = in_CEPinput["complexEvents"][0]["node_ID"]
	str_AttackTree = str_category[0:str_category.index("_")]

	arrayEvents = list()
	correlated_events = dict()
	correlated_events_latest_id = dict() 
	correlated_subgoals = list()

	for event in in_CEPinput["complexEvents"]:
		arrayEvents.append(event["node_ID"])
		cur_event_id = event["complexEvent_ID"]

		if "additionalInfo" in event:
			if "correlated_leaf_nodes" in event["additionalInfo"]:
				for e in event["additionalInfo"]["correlated_leaf_nodes"]:
					logger.debug("{}".format(e))
					corrEvent = e["category"]
					corrEvent = corrEvent[corrEvent.index('#')+1:]
					corrEvent = corrEvent[:corrEvent.index('#')]
					extendedCorrEvents = list(reduce(lambda x,y:x+y,list(map(lambda x:x.split("+"), [corrEvent]))))
					logger.debug("correlated leaves: {}".format(extendedCorrEvents))

					for l in extendedCorrEvents:
						if (l in correlated_events and cur_event_id > correlated_events_latest_id[l]) \
							or l not in correlated_events:
							correlated_events[l] = e["probability"]
							correlated_events_latest_id[l] =  cur_event_id
					
			if "correlated_subgoal_nodes" in event["additionalInfo"]:
				for e in event["additionalInfo"]["correlated_subgoal_nodes"]:
					corrEvent = e["category"]
					corrEvent = corrEvent[corrEvent.index('#')+1:]
					corrEvent = corrEvent[:corrEvent.index('#')]
					extendedCorrEvents = list(reduce(lambda x,y:x+y,list(map(lambda x:x.split("+"), [corrEvent]))))
					extendedCorrEvents = list(filter(lambda x: x not in ctmcModel[str_AttackTree]["leaf_probabilities"], \
																	extendedCorrEvents))
					logger.debug("correlated subgoals: {}".format(extendedCorrEvents))
					correlated_subgoals.extend(extendedCorrEvents)
	

	logger.debug("\n" + "Input events:" + "\n")
	logger.debug(arrayEvents)

	# Create new model attack trees that have nodes associated to the detected events
	countAT = 0
	model_out = {
		"attackModel": [
		]
	}

	# consider unique events only
	extendedArrayEvents=list(reduce(lambda x,y:x+y,list(map(lambda x:x.split("+"),arrayEvents))))
	extendedArrayEvents=list(set(extendedArrayEvents))
	logger.debug("all events: {}".format(str(extendedArrayEvents)))
	for attackTree_i in model["attackModel"]:

		if str_AttackTree!=attackTree_i["attackTree_ID"]:
			continue

		for e in extendedArrayEvents:
			if e not in ctmcModel[str_AttackTree]["place_list"]:
				logger.error("Invalid node id received!")
				raise Exception('Invalid node id received!')

		for s in attackTree_i['subgoal_nodes']:
			if s["node_ID"] in correlated_events:
				extendedArrayEvents.append(s["node_ID"])
			
		attackTree_i["complexEvents"] = extendedArrayEvents
		correlated_subgoals.extend(extendedArrayEvents)
		logger.debug("all events for initial vector: {}".format(str(correlated_subgoals)))
		label=get_place_label(correlated_subgoals, ctmcModel[str_AttackTree]["place_list"])
		a_label=get_aggregated_label(label, ctmcModel[str_AttackTree]["place_list"], str_AttackTree)
		print(a_label)
		for l in attackTree_i['leaf_nodes']:
			i = ctmcModel[str_AttackTree]["place_list"].index(l["node_ID"])
			if(a_label[i]=='0'):
				a_label[i]='1'
		logger.info("aggregated label: {}".format(str(a_label)))
		attackTree_i["current_aggregated_state"] = "".join(a_label)
		model_out["attackModel"].append(attackTree_i)

		mitigation_list=in_CEPinput["atomic_mitigation_list"]
		num_mitigation = len(mitigation_list)
		target_node_ids = list()
		target_places = list()
		target_places_len = list()
		individual_mitigation_rate = list()
		for m in mitigation_list:
			relevant_places=0
			for n in m["target_node_id"]:
				if n in ctmcModel[str_AttackTree]["place_list"]:
					target_node_ids.append(n)
					relevant_places+=1
			target_places_len.append(relevant_places)
			individual_mitigation_rate.append(m["efficiency"] / m["implementation_time_minutes"])
		
		target_places=list()
		for i in target_node_ids:
			target_places.append(ctmcModel[str_AttackTree]["place_list"].index(i))

		label=get_place_label(target_node_ids, ctmcModel[str_AttackTree]["place_list"])
		mitigation_node_children=get_aggregated_label(label, ctmcModel[str_AttackTree]["place_list"], str_AttackTree)
		for l in model_out["attackModel"][0]['leaf_nodes']:
			i = ctmcModel[str_AttackTree]["place_list"].index(l["node_ID"])
			if(mitigation_node_children[i]=='0'):
				mitigation_node_children[i]='1'
		mitigation_node_children = "".join(mitigation_node_children)
		mitigation_node_children=bytes(mitigation_node_children, encoding="utf-8")
		logger.info("mitigation_node_children: {}".format(mitigation_node_children))

		target_places_arr=(ctypes.c_int * len(target_places))(*target_places)
		target_places_len_arr=(ctypes.c_int * len(target_places_len))(*target_places_len)
		individual_mitigation_rate_arr=(ctypes.c_float * len(individual_mitigation_rate))(*individual_mitigation_rate)

		mitigation_model=[num_mitigation, target_places_arr, target_places_len_arr, \
						individual_mitigation_rate_arr, mitigation_node_children]
		break

	if not model_out["attackModel"]:
		logger.error("Co-simulator did not receive an event associated to the attack tree nodes!")
		raise Exception('Co-simulator did not receive an event associated to the attack tree nodes!')

	return [model_out, str_AttackTree, "", [], extendedArrayEvents, correlated_events, mitigation_model]

def parse_events_f8(in_filename, in_attackModel, ctmcModel):
	"""Load JSON format"""
	# load JSON
	in_CEPinput = None   # input object
	with open(in_filename) as file:
		in_CEPinput = json.load(file)

	print("calling parser")
	# parse input object
	in_attackModel = parse_obj_f8(in_CEPinput, in_attackModel, ctmcModel, True)
	in_CEPinput["node_ID"] =  in_attackModel[2]

	#print(in_attackModel)
	return [in_attackModel[0], in_attackModel[4], in_CEPinput, in_attackModel[1], in_attackModel[-2], in_attackModel[-1]]





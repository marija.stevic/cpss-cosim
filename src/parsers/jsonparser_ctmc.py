import json
import ctypes
import api.constants as CONSTANTS
import cacheaccess.cache_utils as cache_utils
from parsers.ctmc_parser_utils import *

ctmc_root=CONSTANTS.get_json_dir()+"/ctmc_models/"

def get_optimized_transition_lists(attack_model):

    optimized_transition_lists=dict()
    leaf_probabilities_dicts=dict()

    for attree in attack_model:

        tree_id=attree["attackTree_ID"]

        leaf_probabilities=dict()
        for leaf in attree["leaf_nodes"]:
            leaf_probabilities[leaf["node_ID"]]=leaf["probability_leaf_node"]
        leaf_probabilities_dicts[tree_id]=leaf_probabilities

        transitions=list()
        #[rate, leaf_node_ids]
        for trans in attree["transitions"]:
            t_list=list()
            t_list.append(1.0/trans['compromise_t'])
            for i in trans["inputPlaces"]:
                if i in leaf_probabilities:
                    t_list.append(i)
            transitions.append(t_list)
        optimized_transition_lists[tree_id]=transitions

    return [optimized_transition_lists, leaf_probabilities_dicts]

def load_ctmc_models(attack_model, tree_names=[]):
    '''returns {"treename":{"place_list", "no_states", "state_transition_matrix", "optimized_transition_lists", "leaf_probabilities"}}'''

    if(cache_utils.test_cache()==False):
        raise Exception("Cache connection not available")
    
    [optimized_transition_lists, leaf_probabilities_dicts]=get_optimized_transition_lists(attack_model)
    ctmc_models=dict()
    for attree in attack_model:

        cur_model=dict()
        tree_id=attree["attackTree_ID"]
        if tree_names and tree_id not in tree_names:
            continue

        populate_prefix_notation(attree)

        ctmc_model = None  
        with open(ctmc_root+"ctmc_"+tree_id+"_p.json") as file:
            ctmc_model = json.load(file)

        cur_model["place_list"] = ctmc_model["place_list"]
        cur_model["optimized_transition_lists"]=optimized_transition_lists[tree_id]
        cur_model["leaf_probabilities"]=leaf_probabilities_dicts[tree_id]

        no_states=len(ctmc_model["aggregate_state_labels"])
        cur_model["no_ctmc_states"]=no_states

        #used to set pi_0
        for i in range(len(ctmc_model["aggregate_state_labels"])):
            key=tree_id + "_" + ctmc_model["aggregate_state_labels"][i]
            value=str(i)
            cache_utils.set_entry(key, value)

        #used for aggregating result
        state_labels=ctmc_model["aggregate_state_labels"]
        for i in range(len(state_labels)):
            state_labels[i]=bytes(state_labels[i], encoding="utf-8")
        state_labels_arr = (ctypes.c_char_p * len(state_labels))(*state_labels)
        cur_model["aggregate_state_labels"]=state_labels_arr

        cur_model["state_transition_matrix"]=dict()
        state_trans_indexes=list(ctmc_model["aggregated_states_transitions"]["indexes"])
        to_indexes=list()
        from_indexes=list()
        transition_indexes=list()
        transition_indexes_len=list()

        k=0
        for tr_k in state_trans_indexes:
            from_state=int(tr_k.split(",")[0].replace("(",""))
            to_state=int(tr_k.split(",")[1].replace(")",""))
            to_indexes.append(to_state)
            from_indexes.append(from_state)

            transition_indexes.extend(ctmc_model["aggregated_states_transitions"]["lists"][k])
            transition_indexes_len.append(len(ctmc_model["aggregated_states_transitions"]["lists"][k]))
            k+=1
        
        #add self to self link indexes
        for i in range(no_states):
            to_indexes.append(i)
            from_indexes.append(i)

        cur_model["state_transition_matrix"]["to_indexes"]=(ctypes.c_int * len(to_indexes))(*to_indexes)
        cur_model["state_transition_matrix"]["from_indexes"]=(ctypes.c_int * len(from_indexes))(*from_indexes)
        cur_model["state_transition_matrix"]["transition_indexes"] = \
            (ctypes.c_int * len(transition_indexes))(*transition_indexes)
        cur_model["state_transition_matrix"]["transition_indexes_len"] = \
            (ctypes.c_int * len(transition_indexes_len))(*transition_indexes_len)

        ctmc_models[tree_id]=cur_model
    
    return ctmc_models

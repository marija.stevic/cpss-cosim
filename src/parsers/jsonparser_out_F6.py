import json
import requests
import time
import os
import logging

from api.constants import get_json_dir
from api.perf_m import timer

logger=logging.getLogger('simulator_logger')

@timer
def dump_cosimout_f6(filename, in_attackModel):
    """Dump results to a JSON-File"""
    with open(filename, "w") as file:
        json.dump(in_attackModel, file)

@timer
def dump_cosimout(filename, content):
    """Dump results to a file"""
    with open(filename, "w") as file:
        file.write(content)

@timer
def post_cosimout_f6(endpoint, in_attackModel, output_type):
    """Send in_attackModel to an http-endpoint (JSON-encoded)."""
    response = None
    if(output_type=='file'):
        filename= get_json_dir() +'/JSON_out_F6_v10_'+str(time.time())+'.json'
        with open(filename, "w") as file:
            json.dump(in_attackModel, file, indent="\t")
        try:
            file = open(filename, "r")
            response = requests.post(endpoint, files={"file": file})
        except Exception as ex:
            logger.error(ex)
        finally:
            os.remove(filename)
    elif(output_type=='string'):
        response = requests.post(endpoint, json=json.dumps(in_attackModel))
    else:
        response = requests.post(endpoint, json=in_attackModel)

    response.raise_for_status() # throw Exception on http-error

import snakes.plugins
snakes.plugins.load("gv", "snakes.nets", "nets")
from nets import *

from graphviz import Digraph
from more_itertools import locate
import logging

logger=logging.getLogger('simulator_logger')

prefix_notations=dict()

def get_place_label(marking, place_list):
    res=[]
    for p in place_list:
        if(p in marking):
            res.append('1')
            #res.append(len(marking(p)))
        else:
            res.append('0')
    return res

def get_all_indexes(dfs_tree, place_list):
    if len(dfs_tree)==1:
        return [place_list.index(dfs_tree[0])]
    #print(dfs_tree)
    indexes=list()
    for i in range(len(dfs_tree)):
        if not isinstance(dfs_tree[i], list):
            indexes.extend([place_list.index(dfs_tree[i])])
        else:
            indexes.extend(get_all_indexes(dfs_tree[i], place_list))
    return indexes

def get_descendent_indexes(sub_tree_root_name, dfs_tree, place_list):
    #print(sub_tree_root_name, dfs_tree)
    if dfs_tree[0]==sub_tree_root_name:
        return get_all_indexes(dfs_tree, place_list)
    else:
        for i in range(1, len(dfs_tree)):
           indexes=get_descendent_indexes(sub_tree_root_name, dfs_tree[i], place_list)
           if indexes!=None:
                return indexes
    return None    

def get_aggregated_label(label, place_list, tree_name):
    #print(label)
    res=['0']*len(label)
    for i in range(len(label)):
        if label[i]=='1' and res[i]=='0':
            descendent_indexes=get_descendent_indexes(place_list[i], prefix_notations[tree_name], place_list)
            #print(descendent_indexes)
            for j in descendent_indexes:
                res[j]='*'
            res[i]='1'

    #print("label: %s, aggr_label: %s" % (str(label), str(res)))
    return res

def dfs_find_add(parent, child, prefix_notation):
    if prefix_notation[0]!=parent:
        for i in range(1,len(prefix_notation)):
            dfs_find_add(parent, child, prefix_notation[i])
    else:
        prefix_notation.append([child])

def get_parent(edge_list, child):

    for i in edge_list:
        if i["parent"]==child:
            return i["child"]
    
    return ""


def populate_prefix_notation(attack_tree):

    global prefix_notations
    prefix_notation=list()
    prefix_notation.append(attack_tree["root"]["node_ID"])

    for i in attack_tree["subgoal_nodes"]:
        dfs_find_add(get_parent(attack_tree["edges"], i["node_ID"]), i["node_ID"], prefix_notation)

    for i in attack_tree["leaf_nodes"]:
        dfs_find_add(get_parent(attack_tree["edges"], i["node_ID"]), i["node_ID"], prefix_notation)

    prefix_notations[attack_tree["attackTree_ID"]]=prefix_notation

    return
 
import json
import logging
from snakes.nets import PetriNet, Place, Transition, Expression, Variable, MultiSet
from snakes.nets import Substitution

logger=logging.getLogger('simulator_logger')

def parse(filename):
    """Load a PetriNet from a JSON-File"""

    # load JSON
    in_places = None   # input object
    with open(filename) as file:
        in_places = json.load(file)

    # Create Petri net
    net = PetriNet('Input-Net')

    # Add transition
    transition = Transition('t', Expression('x < 5'))
    net.add_transition(transition)

    # Add Places
    for in_place in in_places:
        p = Place(in_place['id'], [ in_place['value'] ])
        net.add_place(p)

        # Add arcs
        net.add_input(p.name, transition.name, Variable('x'))
        net.add_output(p.name, transition.name, Expression('x+1'))

    return net

def _place_array(net):
    out = []
    for place in net.place():
        p = {
            "id": place.name,
            "value": place.tokens.items()[0]
        }
        out.append(p)
    return out

def dump(net, filename):
    """Dump a PetriNet to a JSON-File"""
    out = _place_array(net)

    with open(filename, "w") as file:
        json.dump(out, file, indent="\t")
    
def dumps(net):
    """Dump a PetriNet to a JSON-String"""
    out = _place_array(net)
    return json.dumps(out, indent="\t")


import json
from graphviz import Digraph
import logging
import math
import snakes.plugins
snakes.plugins.load("gv", "snakes.nets", "nets")    # line has to remain here
from nets import *
from copy import deepcopy

import api.constants as CONSTANTS
import time
import ctypes
import parsers.ctmc_parser_utils as cthelper
import numpy as np
from api.perf_m import timer

logger=logging.getLogger('simulator_logger')

def parse_attack_model_f2(filename):
    """Load JSON format JSON-AttackModel_F2: attack model as a set of attack paths"""
    # load JSON
    in_attackModel = None   # input object
    with open(filename) as file:
        in_attackModel = json.load(file)

    return in_attackModel

# Uses graphviz to draws attackModel_F2 loaded from JSON file (before sim, use draw_sim_attack_model_f2 to draw model with sim results!)
def draw_attack_model_f2(attackModel_F2):
    for attackTree_i in attackModel_F2["attackModel"]:
        #print("\n" + "Attack tree for threat scenario: ", attackTree_i["attackTree_ID"])

        #print("\n" + "Attack path ID:", attackPath_i["attackPath_ID"])
        # create a graph for each attack path
        ap_graph_i = Digraph(name=attackTree_i["attackTree_ID"],
                             node_attr={'shape': 'octagon', 'style': 'filled', 'fillcolor': 'lightblue1'})
        # add leaf nodes
        for leaf_node_i in attackTree_i["leaf_nodes"]:
            ap_graph_i.node(leaf_node_i["node_ID"],
                            leaf_node_i["node_ID"] + '\n' + 'p = %s' % leaf_node_i["probability_leaf_node"],
                            margin='0.25')

        # add subgoal nodes
        for subgoal_node_i in attackTree_i["subgoal_nodes"]:
            ap_graph_i.node(subgoal_node_i["node_ID"], margin='0.25')

        # add root node
        ap_graph_i.node(attackTree_i["root"]["node_ID"], margin='0.25')

        # add edges
        for edge_i in attackTree_i["edges"]:
            ap_graph_i.edge(edge_i["parent"], edge_i["child"], edge_i["type"])

        #ap_graph_i.edge_attr['style'] = 'dashed'

        # draw graph
        ap_graph_i.format = 'png'
        ap_graph_i.render(CONSTANTS.get_figure_dir() + '/' + attackTree_i["attackTree_ID"] + '___1_in', view=False)

    return None

# Uses graphviz to draw attackModel_F2 that results after sim
def draw_sim_attack_model_f2(attackModel_F2, subname):
    for attackTree_i in attackModel_F2["attackModel"]:

        t_marks = len(attackTree_i["root"]["pr_time"])
        t_marks_list = list(range(0, t_marks, math.ceil(t_marks / 5)));
        if(t_marks_list[-1]!=t_marks-1):
            t_marks_list.append(t_marks-1)

        for t_mark in t_marks_list:

            # create a graph for each attack path
            ap_graph_i = Digraph(name=attackTree_i["attackTree_ID"],
                                 node_attr={'shape': 'octagon', 'style': 'filled', 'fillcolor': 'palevioletred1'})

            t_mark = int(t_mark)

            # add leaf nodes
            for leaf_node_i in attackTree_i["leaf_nodes"]:
                ap_graph_i.node(leaf_node_i["node_ID"],
                                leaf_node_i["node_ID"] + '\n' + 'p = %s' % str(leaf_node_i["pr_time"][t_mark]),
                                margin='0.25')

            # add subgoal nodes
            for subgoal_node_i in attackTree_i["subgoal_nodes"]:
                #print(subgoal_node_i)
                ap_graph_i.node(subgoal_node_i["node_ID"],
                                subgoal_node_i["node_ID"] + '\n' + 'p = %s' % str(subgoal_node_i["pr_time"][t_mark]),
                                #+ '\n' + 't = %s' % subgoal_node_i["time2subgoal"],
                                margin='0.25')

            # add root node
            t_stamp = t_mark*5
            ap_graph_i.node(attackTree_i["root"]["node_ID"],
                            attackTree_i["root"]["node_ID"] + '\n' + 'p = %s' % \
                            attackTree_i["root"]["pr_time"][t_mark] + '\n' + 't = %s' % \
                            + t_stamp,
                            margin='0.25')

            # add edges
            for edge_i in attackTree_i["edges"]:
                ap_graph_i.edge(edge_i["parent"], edge_i["child"], edge_i["type"])

            # draw graph
            ap_graph_i.format = 'png'
            ap_graph_i.render(CONSTANTS.get_figure_dir() + '/' + attackTree_i["attackTree_ID"]
                              + '___3_iter__' + str(t_stamp) + subname, view=False)

    return None

def get_transition_rate(transition_name, transitions):
    for transition in transitions:
        if transition["t_ID"] == transition_name:
            return transition["compromise_t"]
    return -1

def get_leaf_probability(node_name, leaf_nodes):
    for leaf in leaf_nodes:
        if leaf["node_ID"]==node_name:
            return leaf["probability_leaf_node"]
    # this line is fail-safe only
    return 1.0

def get_probability(label_from, label_to, leaf_nodes, nodes, node_levels):
    i=-1
    probability=1.0
    for i in range(len(node_levels)):
        # this means token from  a node is moved in next staged
        # multiple places may participate e.g. AND node
        # now node_level = 1 designates leaf, which may change in future
        if(label_from[i]==1 and label_to[i]==0 and node_levels[i]==1):
            probability*=get_leaf_probability(nodes[i], leaf_nodes)
    return probability

@timer
def create(optimized_transition_list, leaf_probabilities_dict, array_events, place_list, correlated_events):

    logger.info("all events: {}".format(array_events))
    logger.info("correlated_events: {}".format(correlated_events))
    temp=list()
    transition_rate=(ctypes.c_float * len(optimized_transition_list))(*temp)
    temp2=list()
    modified_leaf_probabilities=(ctypes.c_float * len(leaf_probabilities_dict))(*temp2)
    for i in leaf_probabilities_dict:
        marking_index = place_list.index(i)
        if i in array_events:
            modified_leaf_probabilities[marking_index] = 1.0
        elif i in correlated_events:
            modified_leaf_probabilities[marking_index] = correlated_events[i]
        else:
            modified_leaf_probabilities[marking_index] = leaf_probabilities_dict[i]
    for tr_i in range(len(optimized_transition_list)):
        transition_rate[tr_i]=0.0
        tr_entry=optimized_transition_list[tr_i]
        norm_rate=tr_entry[0] # rate
        if len(tr_entry)>1:
            for ii in tr_entry[1:]:
                if ii in array_events:
                    continue
                if ii in correlated_events:
                    norm_rate*=correlated_events[ii]
                elif ii in leaf_probabilities_dict:
                    norm_rate*=leaf_probabilities_dict[ii]
        transition_rate[tr_i] += norm_rate

    return [transition_rate, modified_leaf_probabilities]






import json

from snakes.data import MultiSet
import snakes.plugins
snakes.plugins.load("gv", "snakes.nets", "nets")
from nets import *
from copy import deepcopy
import logging
import cacheaccess.cache_utils as cache_utils
from parsers.ctmc_parser_utils import get_aggregated_label, get_place_label

from functools import reduce
import numpy as np
from api.perf_m import timer

# from snakes.nets import PetriNet, Place, Transition, Expression, Variable, MultiSet
# from snakes.nets import Substitution

logger=logging.getLogger('simulator_logger')

@timer
def parse_obj_f7(in_CEPinput, in_attackModel, ctmcModel, cmd_line=False):
	arrayEvents = []
	recentEventDetails = []

	"""Add input-events to attackModel."""
	model = deepcopy(in_attackModel)    # copy the base model to keep it
	# parse events
	str_category = in_CEPinput["category"]
	str_AttackTree = str_category[(str_category.index("#")+1):str_category.index("_")]
	str_Event = str_category[(str_category.index("_")+1):]
	str_Event = str_Event[:str_Event.index("#")]
	str_Event = str_AttackTree + "_" + str_Event

	# add str_Event to array of events
	arrayEvents.append(str_Event)

	if "additionalInfo" in in_CEPinput:
		if "correlated_leaf_nodes" in in_CEPinput["additionalInfo"]:
			for e in in_CEPinput["additionalInfo"]["correlated_leaf_nodes"]:
				logger.debug("{}".format(e))
				corrEvent = e["category"]
				corrEvent = corrEvent[corrEvent.index('#')+1:]
				corrEvent = corrEvent[:corrEvent.index('#')]
				extendedCorrEvents = list(reduce(lambda x,y:x+y,list(map(lambda x:x.split("+"), [corrEvent]))))
				logger.debug("correlated leaves: {}".format(extendedCorrEvents))
				corrEventProb = e["probability"]
				logger.debug("correlated probability: {}".format(corrEventProb))
				cache_utils.set_correlated_events(str_AttackTree, extendedCorrEvents, corrEventProb)
		if "correlated_subgoal_nodes" in in_CEPinput["additionalInfo"]:
			for e in in_CEPinput["additionalInfo"]["correlated_subgoal_nodes"]:
				corrEvent = e["category"]
				corrEvent = corrEvent[corrEvent.index('#')+1:]
				corrEvent = corrEvent[:corrEvent.index('#')]
				extendedCorrEvents = list(reduce(lambda x,y:x+y,list(map(lambda x:x.split("+"), [corrEvent]))))
				extendedCorrEvents = list(filter(lambda x: x not in ctmcModel[str_AttackTree]["leaf_probabilities"], \
																extendedCorrEvents))
				logger.debug("correlated subgoals: {}".format(extendedCorrEvents))
				if extendedCorrEvents:
					cache_utils.set_correlated_events(str_AttackTree, extendedCorrEvents, 1.0)
		

	correlated_events = dict()
	correlated_subgoals = list()
	cache_utils.get_all_correlated_events(str_AttackTree, correlated_events, correlated_subgoals)
	
	# add recent events
	[recent_events, recentEventDetails] = cache_utils.get_all_relevant_events(str_AttackTree)
	arrayEvents = arrayEvents + recent_events

	logger.debug("\n" + "Input events:" + "\n")
	logger.debug(arrayEvents)

	# Create new model attack trees that have nodes associated to the detected events
	countAT = 0
	model_out = {
		"attackModel": [
		]
	}

	# consider unique events only
	extendedArrayEvents=list(reduce(lambda x,y:x+y,list(map(lambda x:x.split("+"),arrayEvents))))
	extendedArrayEvents=list(set(extendedArrayEvents))
	logger.debug("all events: {}".format(str(extendedArrayEvents)))
	for attackTree_i in model["attackModel"]:

		if str_AttackTree!=attackTree_i["attackTree_ID"]:
			continue

		for e in extendedArrayEvents:
			if e not in ctmcModel[str_AttackTree]["place_list"]:
				logger.error("Invalid node id received!")
				raise Exception('Invalid node id received!')

		for s in attackTree_i['subgoal_nodes']:
			if s["node_ID"] in correlated_events:
				extendedArrayEvents.append(s["node_ID"])
			
		attackTree_i["complexEvents"] = extendedArrayEvents
		correlated_subgoals.extend(extendedArrayEvents)
		label=get_place_label(correlated_subgoals, ctmcModel[str_AttackTree]["place_list"])
		a_label=get_aggregated_label(label, ctmcModel[str_AttackTree]["place_list"], str_AttackTree)
		print(a_label)
		for l in attackTree_i['leaf_nodes']:
			i = ctmcModel[str_AttackTree]["place_list"].index(l["node_ID"])
			if(a_label[i]=='0'):
				a_label[i]='1'
		logger.info("aggregated label: {}".format(str(a_label)))
		attackTree_i["current_aggregated_state"] = "".join(a_label)
		model_out["attackModel"].append(attackTree_i)
		break

	if not model_out["attackModel"]:
		logger.error("Co-simulator did not receive an event associated to the attack tree nodes!")
		raise Exception('Co-simulator did not receive an event associated to the attack tree nodes!')

	return [model_out, str_AttackTree, str_Event, recentEventDetails, extendedArrayEvents, correlated_events]

def parse_events_f7(in_filename, in_attackModel, ctmcModel):
	"""Load JSON format"""
	# load JSON
	in_CEPinput = None   # input object
	with open(in_filename) as file:
		in_CEPinput = json.load(file)

	print("calling parser")
	# parse input object
	[model_out, str_AttackTree, str_Event, recentEventDetails, extendedArrayEvents, correlated_events] = parse_obj_f7(in_CEPinput, in_attackModel, ctmcModel, True)
	in_CEPinput["node_ID"] =  str_Event

	#print(in_attackModel)
	return [model_out, str_AttackTree, str_Event, recentEventDetails, extendedArrayEvents, correlated_events, in_CEPinput]





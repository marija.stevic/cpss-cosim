import numpy as np
import math
import json
import copy
from scipy.integrate import ode
import matplotlib.pyplot as plt

from parsers.jsonparser_out_F6 import *
from parsers.jsonparser_AttackModel_F2 import *

from api.constants import get_json_dir, get_figure_dir, get_append_correlated_events, get_new_norm

import dbaccess.db_utils as db_utils

import logging
import datetime
import ctypes

import api.constants as CONSTANTS
from api.perf_m import timer

logger=logging.getLogger('simulator_logger')

INTEGRATOR_EXECUTABLE_FILE=ctypes.CDLL("/usr/cosim/src/bigc.so")
INTEGRATOR_EXECUTABLE=INTEGRATOR_EXECUTABLE_FILE.main_func
INTEGRATOR_EXECUTABLE.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_float, \
                                    ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), \
                                    ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_int), \
                                    ctypes.POINTER(ctypes.c_int), ctypes.c_int, \
                                    ctypes.POINTER(ctypes.c_float), \
                                    ctypes.POINTER(ctypes.c_float), \
                                    ctypes.c_int, ctypes.POINTER(ctypes.c_char_p))

INTEGRATOR_F8_EXECUTABLE_FILE=ctypes.CDLL("/usr/cosim/src/bigc_f8.so")
INTEGRATOR_F8_EXECUTABLE=INTEGRATOR_F8_EXECUTABLE_FILE.main_func_f8
INTEGRATOR_F8_EXECUTABLE.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_float, \
                        ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_float),\
                        ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), \
                        ctypes.c_int, ctypes.POINTER(ctypes.c_float), \
                        ctypes.c_int, ctypes.POINTER(ctypes.c_int), \
                        ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_float), ctypes.c_char_p, \
                        ctypes.POINTER(ctypes.c_float), ctypes.c_int, ctypes.POINTER(ctypes.c_char_p))

INTEGRATOR2_EXECUTABLE_FILE=ctypes.CDLL("/usr/cosim/src/bigc2.so")
INTEGRATOR2_EXECUTABLE=INTEGRATOR2_EXECUTABLE_FILE.main_func
INTEGRATOR2_EXECUTABLE.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_float, \
                                    ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), \
                                    ctypes.POINTER(ctypes.c_float), ctypes.POINTER(ctypes.c_int), \
                                    ctypes.POINTER(ctypes.c_int), ctypes.c_int, \
                                    ctypes.POINTER(ctypes.c_float), \
                                    ctypes.POINTER(ctypes.c_float), \
                                    ctypes.c_int, ctypes.POINTER(ctypes.c_char_p))

INTEGRATOR2_F8_EXECUTABLE_FILE=ctypes.CDLL("/usr/cosim/src/bigc_f8_2.so")
INTEGRATOR2_F8_EXECUTABLE=INTEGRATOR2_F8_EXECUTABLE_FILE.main_func_f8
INTEGRATOR2_F8_EXECUTABLE.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_float, \
                        ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_float),\
                        ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int), \
                        ctypes.c_int, ctypes.POINTER(ctypes.c_float), \
                        ctypes.c_int, ctypes.POINTER(ctypes.c_int), \
                        ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_float), ctypes.c_char_p, \
                        ctypes.POINTER(ctypes.c_float), ctypes.c_int, ctypes.POINTER(ctypes.c_char_p))

def get_timestamp():
    return str(datetime.datetime.now().time()).replace(':','_')

def infitismal_generator(state_transition_matrix, no_states, absorbing_states):

    Q = np.zeros((no_states, no_states))

    # saves ordering of state indexes in Q matrix, row-wise
    # it is needed as there are multitoken states for which markings are still stored
    state_order = [0] * no_states

    # state 0 i.e. initial state is always valid and is first entry
    index = 1
    # to put absorbing states at the end
    tail_index = no_states - 1
    
    for vec in state_transition_matrix.keys():
        # insert state index in ordering list
        if(vec[0] not in state_order):
            if(vec[0] not in absorbing_states):
                state_order[index] = vec[0]
                index += 1
            else:
                state_order[tail_index] = vec[0]
                tail_index -= 1
        
        if(vec[1] not in state_order):
            if(vec[1] not in absorbing_states):
                state_order[index] = vec[1]
                index += 1
            else:
                state_order[tail_index] = vec[1]
                tail_index -= 1
        
        # populate values in Q according to the order
        Q[state_order.index(vec[0]), state_order.index(vec[1])] += state_transition_matrix[vec]

    for i in range(no_states):
        #Q[i,i] is also being summed up
        Q[i,i] = 2*Q[i,i] - np.sum(Q[i, :])

    return (Q, state_order)

def steady_state_calculator(no_states, Q):

    b = np.zeros(no_states+1,1)
    QQ = np.ones(1, no_states)

    A = np.concatenate(np.transpose(Q), QQ)

    A_transposed = np.transpose(A)
    intermediate_res = np.matmul(A_transposed, A)
    intermediate_res = np.linalg.solve(intermediate_res, np.eye(len(intermediate_res)))
    intermediate_res = np.matmul(intermediate_res, A_transposed)
    steady_st_prob = np.matmul(intermediate_res, b)

    return steady_st_prob

def diff_eqns(t, pi_t, Q):

    no_states = len(Q)
    dpidt = np.zeros(no_states)
   
    for i in range(no_states): 
        for j in range(no_states):
            dpidt[i] += pi_t[j]*Q[j,i]
        
    return dpidt

def state_occupancy_time(Q, pi_0, no_states, absorbing_states):

    no_absorbing_states = len(absorbing_states)
    time_to_absorption = np.zeros(no_absorbing_states)                                                                     
    pi_abs_infinity = np.zeros(no_absorbing_states)                                                                        
    time_to_absorption_numerator = np.zeros(no_absorbing_states)
    logger.debug("initialized values for occupancy calculation")                                                     
    
    # Generating matrices required for calculating time_to_absorption 
    # and time_present_in_transient_states
    num_transient_states = no_states - no_absorbing_states               
    Q_u = Q[0:num_transient_states, 0:num_transient_states]      
    pi_0_u = pi_0[0:num_transient_states] 
    inv_Q_u = -1 * np.linalg.solve(Q_u, np.eye(len(Q_u)))     
    e = np.ones(no_states-1)
    logger.debug("defined values for occupancy calculation") 

    # Calculation of time_present_in_transient_states before absorption
    # mean time spent in transient state
    time_present_in_transient_states = np.matmul(pi_0_u, inv_Q_u)
    logger.debug("calculated time_present_in_transient_states: {}".format(str(time_present_in_transient_states))) 

    if no_absorbing_states!=1 :   
        for i in range(no_absorbing_states):
            # Calculate the final probability of absorption to 'i'th absorption state 
            pi_abs_infinity[i] = np.matmul(time_present_in_transient_states,\
                 Q[0:num_transient_states, num_transient_states+i])

        # Calculation of time_to_absorption
        # TODO: review translates to (pi_0_u * inv_Q_u * inv_Q_u * Q(part))
        intermediate_res = np.matmul(time_present_in_transient_states, inv_Q_u)

        for i in range(no_absorbing_states):
            # Calculate the numerator of time to absorption to 'i'th absorbing state
            time_to_absorption_numerator[i] = np.matmul(intermediate_res, \
                Q[0:num_transient_states, num_transient_states+i])
            # Mean time to absorbing in 'i'th absorbing state
            time_to_absorption[i] = time_to_absorption_numerator[i]/pi_abs_infinity[i]
    else:
        # Mean time to absorption
        time_to_absorption = np.matmul(time_present_in_transient_states, np.transpose(e))

    return (time_to_absorption, time_present_in_transient_states)        

@timer
def simulate_ctmc(no_states, individual_transition_rates, num_leaves, modified_leaf_probabilities, \
                    to_state_indexes, from_state_indexes, \
                    transition_indexes, transition_indexes_len, initial_state_index, time_horizon, \
                        no_places, aggregate_labels, result):

    logger.debug("starting simulation...")
    
    if(get_new_norm()):
        INTEGRATOR2_EXECUTABLE(no_states, len(to_state_indexes), int(initial_state_index), float(time_horizon),\
                     to_state_indexes, from_state_indexes, individual_transition_rates, \
                        transition_indexes, transition_indexes_len, num_leaves, modified_leaf_probabilities, \
                            result, no_places, aggregate_labels)
    else:
        INTEGRATOR_EXECUTABLE(no_states, len(to_state_indexes), int(initial_state_index), float(time_horizon),\
                     to_state_indexes, from_state_indexes, individual_transition_rates, \
                        transition_indexes, transition_indexes_len, num_leaves, modified_leaf_probabilities, \
                            result, no_places, aggregate_labels)
    for j in range(5):
	    logger.debug("result: {}".format(result[-5+j]))
    
    return

@timer
def simulate_ctmc_f8(no_states, individual_transition_rates, num_leaves, modified_leaf_probabilities, \
                    to_state_indexes, from_state_indexes, \
                    transition_indexes, transition_indexes_len, initial_state_index, time_horizon, \
                        no_places, aggregate_labels, result, mitigation_model):

    logger.debug("starting simulation f8...")
    
    if(get_new_norm()):
        INTEGRATOR2_F8_EXECUTABLE(no_states, len(to_state_indexes), int(initial_state_index), float(time_horizon),\
                     to_state_indexes, from_state_indexes, individual_transition_rates, \
                        transition_indexes, transition_indexes_len, \
                        num_leaves, modified_leaf_probabilities, \
                        mitigation_model[0], mitigation_model[1], mitigation_model[2], \
                        mitigation_model[3], mitigation_model[4], \
                        result, no_places+1, aggregate_labels)
    else:
        INTEGRATOR_F8_EXECUTABLE(no_states, len(to_state_indexes), int(initial_state_index), float(time_horizon),\
                     to_state_indexes, from_state_indexes, individual_transition_rates, \
                        transition_indexes, transition_indexes_len, \
                        num_leaves, modified_leaf_probabilities, \
                        mitigation_model[0], mitigation_model[1], mitigation_model[2], \
                        mitigation_model[3], mitigation_model[4], \
                        result, no_places+1, aggregate_labels)
    for j in range(5):
	    logger.debug("result: {}".format(result[-5+j]))
    
    return

def down_sample(time_series, time_vector):
    result = list()
    for i in time_vector:
        result.append(abs(round(time_series[i],5)))
    
    return result

@timer
def create_F6_response(attackTree, request_body, recentEvents, sim_result, time_vector, request_type):

    logger.debug("starting f6 building")
    data_F6 = dict()
    data_F6["attackTrees"] = list()
    complexEvents = list()

    if request_type=='f7':
        # events in cache
        if(recentEvents!=None):
            for recentEvent in recentEvents:
                #logger.debug("recent event: {}".format(str(recentEvent)))
                recentEvent=json.loads(recentEvent)
                recentEventNodes=list(reduce(lambda x,y:x+y,\
                    list(map(lambda x:x.split("+"),[recentEvent["node_ID"]]))))
                for node in recentEventNodes:
                    complexEvent = {
                        "node_ID": node,
                        "time_stamp": recentEvent["startTime"],
                        "complexEvent_ID": recentEvent["id"]
                    }
                    if get_append_correlated_events() and "additionalInfo" in recentEvent:
                        complexEvent["additionalInfo"] = recentEvent["additionalInfo"]
                    complexEvents.append(complexEvent)
        
        #logger.debug(str(request_body))
        #current event
        currentEventNodes=list(reduce(lambda x,y:x+y,\
                    list(map(lambda x:x.split("+"),[request_body["node_ID"]]))))
        for node in currentEventNodes:
            complexEvent = {
                    "node_ID": node,
                    "time_stamp": request_body["startTime"],
                    "complexEvent_ID": request_body["id"]
                }
            if get_append_correlated_events() and "additionalInfo" in request_body:
                    complexEvent["additionalInfo"] = request_body["additionalInfo"]
            complexEvents.append(complexEvent)
    else:
        complexEvents=request_body["complexEvents"]
        request_body["startTime"]=""
        data_F6["reevaluation_id"]=request_body["reevaluation_id"]

    add1_data_f6 = {
        "attackTree_ID": attackTree["attackTree_ID"],
        "complexEvents": complexEvents,
        "sim_output": [
        ]
    }
    data_F6["attackTrees"].append(add1_data_f6)

    request_body_events=request_body["node_ID"].split("+")
    add_subgoal_nodes = list()
    for subgoal_node_i in attackTree["subgoal_nodes"]:
        add_subgoal_node_i = {
            "node_ID": subgoal_node_i["node_ID"],
            "goal": subgoal_node_i["goal"],
            "probability_vector": down_sample(sim_result[subgoal_node_i["node_ID"]], time_vector)
        }

        add_subgoal_nodes.append(add_subgoal_node_i)

    add_leaf_nodes = list()
    for leaf_node_i in attackTree["leaf_nodes"]:
        add_leaf_node_i = {
            "node_ID": leaf_node_i["node_ID"],
            "goal": leaf_node_i["goal"],
            "probability_vector": down_sample(sim_result[leaf_node_i["node_ID"]], time_vector)
        }

        add_leaf_nodes.append(add_leaf_node_i)

    logger.debug("inserting root in f6")
    root_node = {
            "node_ID": attackTree["root"]["node_ID"],
            "node_name": attackTree["root"]["node_name"],
            "probability_vector": down_sample(sim_result[attackTree["root"]["node_ID"]], time_vector)
        }
    if(root_node["node_ID"] in request_body_events):
            root_node["targeted_infra_attr"]=targeted_infra_attr
    add2_data_f6 = {
        "time_vector": time_vector,
        "time_ref": request_body["startTime"],
        "root": root_node,
        "subgoal_nodes": add_subgoal_nodes,
        "leaf_nodes": add_leaf_nodes
        }
    data_F6["attackTrees"][0]["sim_output"]=add2_data_f6
    logger.debug("inserted root in f6")

    data_F6_persist=copy.deepcopy(data_F6)
    if(request_type=='f8'):
        safe_node = {
            "node_ID": "safe",
            "node_name": "safe",
            "probability_vector": down_sample(sim_result["safe"], time_vector)
        }
        data_F6_persist["attackTrees"][0]["sim_output"]["safe"] = safe_node    

    return [data_F6, data_F6_persist]

@timer
def make_sim_result(probability_time_series, place_list, total_sim_time, sim_result, request_type):

    no_steps=total_sim_time+1
    for i in range(len(place_list)):
        sim_result[place_list[i]] = probability_time_series[i*no_steps:(i+1)*no_steps]

    if(request_type=='f8'):
        sim_result["safe"] = probability_time_series[len(place_list)*no_steps:]

    return

@timer
def simulate(in_attackModel_F2, output_type, out_endpoint, output_path, \
                            request_type, treename, request_body, recentEvents,\
                                initial_state_index, individual_transition_rates, \
                                    modified_leaf_probabilities, \
                                    to_state_indexes, from_state_indexes, transition_indexes, \
                                        transition_indexes_len,no_states, place_list, aggregate_labels, mitigation_model=None):
    """Simulation CTMC"""
    logger.info("Start simulation")

    # max expected total sim time
    total_sim_time = in_attackModel_F2["attackModel"][0]["time_horizon"] 
    logger.debug("total simulation time: {}".format(total_sim_time))

    res=list()
    no_places=len(place_list)
    num_leaves=len(in_attackModel_F2["attackModel"][0]["leaf_nodes"])
    probability_time_series = None
    if(request_type=='f7'):
        probability_time_series = (ctypes.c_float * (no_places*(total_sim_time+1)))(*res)
        simulate_ctmc(\
        no_states, individual_transition_rates, num_leaves, modified_leaf_probabilities, to_state_indexes, \
            from_state_indexes, transition_indexes, transition_indexes_len, \
                initial_state_index, total_sim_time, no_places, aggregate_labels, probability_time_series)
    else:
        probability_time_series = (ctypes.c_float * ((no_places+1)*(total_sim_time+1)))(*res)
        simulate_ctmc_f8(\
        no_states, individual_transition_rates, num_leaves, modified_leaf_probabilities, to_state_indexes, \
            from_state_indexes, transition_indexes, transition_indexes_len, \
                initial_state_index, total_sim_time, no_places, aggregate_labels, \
                    probability_time_series, mitigation_model)
    
    
    logger.debug("CTMC analysis completed.")

    logger.debug("debug status:{}".format(CONSTANTS.get_sim_debug()))
    logger.debug("figure dir:{}".format(CONSTANTS.get_figure_dir()))
    # NOTE only works if called from a single thread in a process
    # i.e. matplotlib is not thread-safe
    # Gunicorn enables 2 processes as of now
    if(CONSTANTS.get_sim_debug()):
        for i in range(len(probability_time_series)):
            logger.debug("added plot for state:{}".format(i))
            plt.plot(probability_time_series[i], label = str(i))
            
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),
          ncol=3, fancybox=True, shadow=True)
        plt.grid()
        
        plt.savefig(CONSTANTS.get_figure_dir() + '/' + in_attackModel_F2["attackModel"][0]["attackTree_ID"]\
             + "_state_probabilities_" + get_timestamp() + ".png")
        plt.clf()

    # convert state representation to attack tree node representation
    sim_result = dict()
    make_sim_result(probability_time_series, place_list, total_sim_time, sim_result, request_type)
    logger.debug("simultation result length:{}".format(len(sim_result)))
    logger.debug("time series length:{}".format(len(sim_result.get(place_list[0]))))

    # Save raw results in a file (all attack trees and time samples in attackModel)
    out_filename = CONSTANTS.get_json_dir() + "/" + output_path + "/out_raw_results_" + \
            in_attackModel_F2["attackModel"][0]["attackTree_ID"] + "_" + get_timestamp() + ".json"
    if(CONSTANTS.get_reduced_record()==False):
        dump_cosimout_f6(out_filename, sim_result)

    # 3. Create and send F6 response
    step_size = 1
    time_vector = list(range(0, total_sim_time, step_size))
    if time_vector[-1]!=total_sim_time:
        time_vector.append(total_sim_time)
    [f6_response, f6_persist] = create_F6_response(in_attackModel_F2["attackModel"][0], \
        request_body, recentEvents, sim_result, time_vector, request_type)

    logger.debug("Simulation results ready in json-f6 form")

    # send to http-endpoint
    if (out_endpoint != None):
        try:
            logger.info("Send out result")
            post_cosimout_f6(out_endpoint, f6_response, output_type)
        except Exception as ex:
            logger.info("Error sending JSON: " + str(ex))
    
    # Dump cosim results to out JSON-F6
    out_filename = CONSTANTS.get_json_dir() + "/" + output_path + "/out_f6_" + \
        in_attackModel_F2["attackModel"][0]["attackTree_ID"] + "_" + get_timestamp() + ".json"
    if(CONSTANTS.get_no_record()==False):
        logger.debug("writting f6 response")
        dump_cosimout_f6(out_filename, f6_persist)

    return
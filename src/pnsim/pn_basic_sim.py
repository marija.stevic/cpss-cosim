from snakes.data import MultiSet
import snakes.plugins
snakes.plugins.load("gv", "snakes.nets", "nets")
from nets import *
import numpy as np
import math
import json

from parsers.jsonparser_out_F6 import *
from parsers.jsonparser_AttackModel_F2 import *

from api.constants import get_json_dir
from api.constants import get_figure_dir

import dbaccess.db_utils as db_utils

import logging
import datetime
from functools import reduce

logger=logging.getLogger('simulator_logger')

def get_timestamp():
    return str(datetime.datetime.now().time()).replace(':','_')

def get_ref_time():
    return datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")

# Simulate PN
def sim_pn(pn, trans, attackPath_ID):
    #print("\n" + "Simulation begins!")
    #pn.draw(FIGURE_DIR + '/sim_pn_init_%s.png' % attackPath_ID)
    t_enabled = True
    num_transition = 0
    while t_enabled == True:
        modes_pn = []
        for t in trans:
            m_t = t.modes()
            modes_pn.extend(m_t)
        if not modes_pn:
            t_enabled = False
            #print("No modes available!")
        else:
            #print("There are modes: ", modes_pn)
            for t_i in trans:
                mode_i = t_i.modes()
                if mode_i:
                    t_i.fire(mode_i[0])
                    num_transition += 1
                    #pn.draw(FIGURE_DIR + '/sim_pn_%s_%d.png' % attackPath_ID % num_transition)
                    #pn.draw(FIGURE_DIR + '/sim_pn_%d.png' % num_transition)
    return pn, trans


# Monte Carlo Sim
def mc_sim_pn(numMCRuns, in_attackModel):

    sim_result = dict()
    #print("\n" + "Simulation begins!")
    # MC loop
    logger.info("starting simulation.")
    for attackTree_i in in_attackModel["attackModel"]:
        # find end sim time = sum(compromise time)
        total_sim_time = 0 # max expected total sim time
        sim_time_max_MCrun = 0 # save across MC runs max sim time
        for trans_i in attackTree_i["transitions"]:
            total_sim_time = total_sim_time + trans_i["compromise_t"]
        total_sim_time = total_sim_time*2

        # generate time vector
        attackTree_i["time_step"] = 1.0
        attackTree_i["time_t"] = np.arange(0, total_sim_time, attackTree_i["time_step"])

        # Save a list of probabilities of token markings for leaf nodes
        for leaf_node_i in attackTree_i["leaf_nodes"]:
            pr_i = leaf_node_i["probability_leaf_node"]
            list_pr = np.random.binomial(1, pr_i, numMCRuns)
            leaf_node_i["list_pr"] = list_pr

        # Vector with sum of markings of each time instant for all MC runs
        attackTree_i["root"]["marking_sum"] = np.zeros(total_sim_time + 1)
        for subgoal_node_i in attackTree_i["subgoal_nodes"]:
            subgoal_node_i["marking_sum"] = np.zeros(total_sim_time + 1)
        for leaf_node_i in attackTree_i["leaf_nodes"]:
            leaf_node_i["marking_sum"] = np.zeros(total_sim_time + 1)

        # Simulation
        for MCRun_i in range(numMCRuns):
            # Initialization
            # Vector with marking at each time instant per MC run
            attackTree_i["root"]["marking_t"] = np.zeros(total_sim_time + 1)
            for subgoal_node_i in attackTree_i["subgoal_nodes"]:
                subgoal_node_i["marking_t"] = np.zeros(total_sim_time + 1)
            for leaf_node_i in attackTree_i["leaf_nodes"]:
                leaf_node_i["marking_t"] = np.zeros(total_sim_time + 1)

            # Copy PN models and trans
            pn_orgn = attackTree_i["pn_init_model"]
            pn = pn_orgn.copy('pn')
            # trans_orgn = attackTree_i["pn_init_trans"]
            trans = list()
            trans_dict = pn._trans
            for key, value in trans_dict.items():
                trans.append(value)

            # Set initial marking of leaf nodes based on list_pr
            for leaf_node_i in attackTree_i["leaf_nodes"]:
                pr_i = leaf_node_i["list_pr"][MCRun_i]
                if pr_i:
                    pn.place(leaf_node_i["node_ID"]).tokens = MultiSet([dot])
                    leaf_node_i["marking_t"][0] = 1

            # Initial marking finalized: tokens from events from JSON_F7 + tokens based on probabilities
            # Snapshot of initial markings
            # Root node
            place_i = attackTree_i["root"]["node_ID"]
            marking_i = pn.place(place_i).tokens
            if marking_i:
                attackTree_i["root"]["marking_t"][0] = 1

            for subgoal_node_i in attackTree_i["subgoal_nodes"]:
                place_i = subgoal_node_i["node_ID"]
                marking_i = pn.place(place_i).tokens
                if marking_i:
                    subgoal_node_i["marking_t"][0] = 1

            for leaf_node_i in attackTree_i["leaf_nodes"]:
                place_i = leaf_node_i["node_ID"]
                marking_i = pn.place(place_i).tokens
                if marking_i:
                    leaf_node_i["marking_t"][0] = 1

            t_enabled = True
            num_transition = 0 # just for print, not used in sim
            sim_time = 0 # sim for this MC run starts
            # Simulation Run
            while t_enabled == True: # while there are transitions enabled
                modes_pn = []
                for t in trans:
                   m_t = t.modes()
                   modes_pn.extend(m_t)
                if not modes_pn:
                   t_enabled = False
                   #print("No modes available!")
                else:
                   #print("There are modes: ", modes_pn)
                   for t_i in trans:
                       mode_i = t_i.modes()
                       if mode_i:
                           t_i.fire(mode_i[0])

                           #pn.draw(CONSTANTS.get_figure_dir() + '/mc_sim_pn_%s_trns%d.png' % (t_i.name, num_transition))
                           num_transition += 1

                            # find compromise time for t_i
                           trans_ID = t_i.name
                           list_trans = attackTree_i["transitions"]
                           for trans_item in list_trans:
                               if trans_ID == trans_item["t_ID"]:
                                   t_i_time = trans_item["compromise_t"]


                           # Snapshot of markings == 1 (do not reset in case of == 0) and add t_i time to total_time_i
                           # Root node
                           place_i = attackTree_i["root"]["node_ID"]
                           marking_i = pn.place(place_i).tokens # marking of the place after the transition
                           marking_sim_time = attackTree_i["root"]["marking_t"][sim_time]
                           if marking_i:  # future after sim_time + compromise time
                               if marking_sim_time:
                                   for i in range((sim_time+1), (sim_time+t_i_time+1)):
                                       attackTree_i["root"]["marking_t"][i] = 1
                               else:
                                   attackTree_i["root"]["marking_t"][sim_time+t_i_time] = 1
                           else:
                               if marking_sim_time:
                                   for i in range((sim_time+1), (sim_time+t_i_time)):
                                       attackTree_i["root"]["marking_t"][i] = 1

                            # subgoal nodes
                           for subgoal_node_i in attackTree_i["subgoal_nodes"]:
                               place_i = subgoal_node_i["node_ID"]
                               marking_i = pn.place(place_i).tokens  # marking of the place after the transition
                               marking_sim_time = subgoal_node_i["marking_t"][sim_time]
                               if marking_i:  # future after sim_time + compromise time
                                   if marking_sim_time:
                                       for i in range((sim_time + 1), (sim_time + t_i_time + 1)):
                                           subgoal_node_i["marking_t"][i] = 1
                                   else:
                                        subgoal_node_i["marking_t"][sim_time + t_i_time] = 1
                               else:
                                   if marking_sim_time:
                                       for i in range((sim_time + 1), (sim_time + t_i_time)):
                                           subgoal_node_i["marking_t"][i] = 1

                           for leaf_node_i in attackTree_i["leaf_nodes"]:
                               place_i = leaf_node_i["node_ID"]
                               marking_i = pn.place(place_i).tokens  # marking of the place after the transition
                               marking_sim_time = leaf_node_i["marking_t"][sim_time]
                               if marking_i:  # future after sim_time + compromise time
                                   if marking_sim_time:
                                       for i in range((sim_time + 1), (sim_time + t_i_time + 1)):
                                           leaf_node_i["marking_t"][i] = 1
                                   else:
                                       leaf_node_i["marking_t"][sim_time + t_i_time] = 1
                               else:
                                   if marking_sim_time:
                                       for i in range((sim_time + 1), (sim_time + t_i_time)):
                                           leaf_node_i["marking_t"][i] = 1

                            # advance sim_time
                           sim_time = sim_time + t_i_time

            # Post processing - after each MC run (not after each transition!)
            sim_time_max_MCrun = max(sim_time_max_MCrun, sim_time)
            attackTree_i["root"]["marking_sum"] = np.add(attackTree_i["root"]["marking_sum"],\
                                                         attackTree_i["root"]["marking_t"])
            for subgoal_node_i in attackTree_i["subgoal_nodes"]:
                subgoal_node_i["marking_sum"] = np.add(subgoal_node_i["marking_sum"], subgoal_node_i["marking_t"])
            for leaf_node_i in attackTree_i["leaf_nodes"]:
                leaf_node_i["marking_sum"] = np.add(leaf_node_i["marking_sum"], leaf_node_i["marking_t"])

        # post processing after all MC runs are finalized:
        attackTree_i["sim_time_max"] = sim_time_max_MCrun
        logger.debug("Calculate probabilities and prepare results for a user")
        # remove PN objects
        del attackTree_i["pn_init_model"]
        del attackTree_i["pn_init_trans"]
        #del attackTree_i["edges"]
        del attackTree_i["transitions"]

        # remove time instants after sim_time_max_MCrun
        attackTree_i["time_t"] = attackTree_i["time_t"][:(sim_time_max_MCrun+1)]
        attackTree_i["time_t"] = attackTree_i["time_t"].tolist()

        # Calculate probabilities after all MC runs are finished
        attackTree_i["root"]["pr_time"] = np.zeros(sim_time_max_MCrun + 1)
        # remove time instants after sim_time_max_MCrun
        attackTree_i["root"]["marking_sum"] = attackTree_i["root"]["marking_sum"][:(sim_time_max_MCrun+1)]
        attackTree_i["root"]["pr_time"] = attackTree_i["root"]["marking_sum"]/numMCRuns
        # remove ndarrays that are not needed,
        del attackTree_i["root"]["marking_sum"]
        del attackTree_i["root"]["marking_t"]
        # convert pr_time to a list
        attackTree_i["root"]["pr_time"] = attackTree_i["root"]["pr_time"].tolist()
        logger.info("ended simulation.")
        sim_result[str(attackTree_i["root"]['node_ID'])] = attackTree_i["root"]["pr_time"]

        for subgoal_node_i in attackTree_i["subgoal_nodes"]:
            subgoal_node_i["pr_time"] = np.zeros(sim_time_max_MCrun + 1)
            subgoal_node_i["marking_sum"] = subgoal_node_i["marking_sum"][:(sim_time_max_MCrun+1)]
            subgoal_node_i["pr_time"] = subgoal_node_i["marking_sum"]/numMCRuns
            del subgoal_node_i["marking_sum"]
            del subgoal_node_i["marking_t"]
            # convert pr_time to a list
            subgoal_node_i["pr_time"] = subgoal_node_i["pr_time"].tolist()
            sim_result[str(subgoal_node_i['node_ID'])] = subgoal_node_i["pr_time"]

        for leaf_node_i in attackTree_i["leaf_nodes"]:
            leaf_node_i["pr_time"] = np.zeros(sim_time_max_MCrun + 1)
            leaf_node_i["marking_sum"] = leaf_node_i["marking_sum"][:(sim_time_max_MCrun+1)]
            leaf_node_i["pr_time"] = leaf_node_i["marking_sum"]/numMCRuns
            del leaf_node_i["marking_sum"]
            del leaf_node_i["marking_t"]
            del leaf_node_i["list_pr"]
            # convert pr_time to a list
            leaf_node_i["pr_time"] = leaf_node_i["pr_time"].tolist()
            sim_result[str(leaf_node_i['node_ID'])] = leaf_node_i["pr_time"]

    logger.info("\n" + "Simulation for attack tree " + attackTree_i["attackTree_ID"] + " finished " + "\n")
    return [in_attackModel, sim_result]

def simulate(in_attackModel_F2, output_type, out_endpoint, output_path, request_type, treename, request_body, recentEvents):
    """Simulation Petri Nets"""
    logger.info("Start simulation")
    #for attackTree_i in in_attackModel_F2["attackModel"]:
    #   attackPath_index = 0
    #   for attackPath_i in attackTree_i["attackPaths"]:
    #        print("\n" + "Attack path ID:", attackPath_i["attackPath_ID"])
    #        my_pn = in_attackModel_F2["attackModel"][0]["attackPaths"][attackPath_index]["pn_init_model"]
    #        pn_t = in_attackModel_F2["attackModel"][0]["attackPaths"][attackPath_index]["pn_init_trans"]
    #        my_pn, pn_t = sim_pn(my_pn, pn_t, attackPath_i["attackPath_ID"])
    #        #in_attackModel_F2["attackModel"][0]["attackPaths"][attackPath_index]["pn_model"] = my_pn
    #        #in_attackModel_F2["attackModel"][0]["attackPaths"][attackPath_index]["pn_trans"] = pn_t
    #        attackPath_index += 1

    event_id = None
    if(out_endpoint!=None):
      if(request_type=='f7'):
          logger.info("inserting f7 event")
          event_id = db_utils.insert_compromise_event(treename, request_body)
          logger.info("inserted f7 event with id {}.".format(event_id))
      else:
          event_id = db_utils.insert_countermeasure_event(treename, request_body)
          logger.info("inserted f8 event with id {}.".format(event_id))

    # Monte Carlo Simulation Petri Nets
    numMCRuns = 1250
    [in_attackModel_F2, sim_result] = mc_sim_pn(numMCRuns, in_attackModel_F2)
    logger.info("Simulation finished!")

    # Save raw results in a file (all attack trees and time samples in attackModel)
    out_filename = CONSTANTS.get_json_dir() + "/" + output_path + "/out_raw_results_" + \
            in_attackModel_F2["attackModel"][0]["attackTree_ID"] + "_" + get_timestamp() + ".json"
    if(CONSTANTS.get_no_record()==False and CONSTANTS.get_reduced_record()==False):
        dump_cosimout_f6(out_filename, in_attackModel_F2)

    if(out_endpoint!=None):
      db_utils.insert_simulation_result(sim_result, event_id, request_type, treename)
      logger.info("inserted simulation result in database.")

    # create new object for out-json-f6
    data_F6 = {
        "attackTrees": [
        ]
    }

    complexEvents_Ids=""

    for ii, attackTree_i in zip(range(len(in_attackModel_F2["attackModel"])), in_attackModel_F2["attackModel"]):
        # Select time instants for json-f6
        time_step_res = 5
        max_time_samples_res = 50
        if math.floor(attackTree_i["sim_time_max"] / time_step_res) > max_time_samples_res:
            time_step_res = math.floor(attackTree_i["sim_time_max"]/max_time_samples_res)

        # remove fields that are not needed
        del attackTree_i["sim_time_max"]
        del attackTree_i["time_step"]

        # downsample results
        attackTree_i["time_t"] = attackTree_i["time_t"][::time_step_res]
        attackTree_i["root"]["pr_time"] = attackTree_i["root"]["pr_time"][::time_step_res]
        for subgoal_node_i in attackTree_i["subgoal_nodes"]:
            subgoal_node_i["pr_time"] = subgoal_node_i["pr_time"][::time_step_res]
        for leaf_node_i in attackTree_i["leaf_nodes"]:
            leaf_node_i["pr_time"] = leaf_node_i["pr_time"][::time_step_res]

        # Save downsampled results for this attackTree_i in a file
        out_filename = CONSTANTS.get_json_dir() + "/" + output_path + "/out_" + attackTree_i["attackTree_ID"] + "_" +\
                       get_timestamp() +"_downsampled_results.json"
        if(CONSTANTS.get_no_record()==False):
            dump_cosimout_f6(out_filename, attackTree_i)

        logger.debug("starting f6 building")
        complexEvents = []
        if(recentEvents!=None):
            for recentEvent in recentEvents:
                logger.debug("recent event: {}".format(str(recentEvent)))
                recentEvent=json.loads(recentEvent)
                # events in cache
                complexEvent = {
                    "node_ID": recentEvent["node_ID"],
                    "time_stamp": recentEvent["startTime"],
                    "complexEvent_ID": recentEvent["id"]
                }
                complexEvents.append(complexEvent)
        
        #logger.debug(str(request_body))
        #current event
        complexEvent = {
                "node_ID": request_body["node_ID"],
                "time_stamp": request_body["startTime"],
                "complexEvent_ID": request_body["id"]
            }
        complexEvents.append(complexEvent)

        add1_data_f6 = {
            "attackTree_ID": attackTree_i["attackTree_ID"],
            "complexEvents": complexEvents,
            "sim_output": [
            ]
        }
        data_F6["attackTrees"].append(add1_data_f6)
        complexEvents_Ids = complexEvents_Ids + "_" + attackTree_i["complexEvents"][-1]
        logger.debug("added complex event to f6")

        targeted_infra_attr= {}
        targeted_infra_attr["ceiId"]=request_body["ceiId"]
        targeted_infra_attr["ceiElements"]=request_body["ceiElements"]

        add_subgoal_nodes = [
        ]
        for subgoal_node_i in attackTree_i["subgoal_nodes"]:
            add_subgoal_node_i = {
                "node_ID": subgoal_node_i["node_ID"],
                "goal": subgoal_node_i["goal"],
                "probability_vector": subgoal_node_i["pr_time"]
            }

            if(subgoal_node_i["node_ID"]==request_body["node_ID"]):
                add_subgoal_node_i["targeted_infra_attr"]=targeted_infra_attr
            add_subgoal_nodes.append(add_subgoal_node_i)

        add_leaf_nodes = [
        ]
        for leaf_node_i in attackTree_i["leaf_nodes"]:
            add_leaf_node_i = {
                "node_ID": leaf_node_i["node_ID"],
                "goal": leaf_node_i["goal"],
                "probability_vector": leaf_node_i["pr_time"]
            }

            if(leaf_node_i["node_ID"]==request_body["node_ID"]):
                add_leaf_node_i["targeted_infra_attr"]=targeted_infra_attr
            add_leaf_nodes.append(add_leaf_node_i)

        logger.debug("inserting root in f6")
        root_node = {
                "node_ID": attackTree_i["root"]["node_ID"],
                "node_name": attackTree_i["root"]["node_name"],
                "probability_vector": attackTree_i["root"]["pr_time"]
            }
        if(root_node["node_ID"]==request_body["node_ID"]):
                root_node["targeted_infra_attr"]=targeted_infra_attr
        add2_data_f6 = {
            "time_vector": attackTree_i["time_t"],
            "time_ref": request_body["startTime"],
            "root": root_node,
            "subgoal_nodes": add_subgoal_nodes,
            "leaf_nodes": add_leaf_nodes
            }
        data_F6["attackTrees"][(ii)]["sim_output"] = add2_data_f6
        logger.debug("inserted root in f6")

        # Visualize attack tree with probabilities at selected time samples
        # msvTODO: TO BE CORRECTED FOR THE NEW FORMAT OF ATTACK TREES
        logger.debug("draw sim result: %s", str(CONSTANTS.get_sim_debug()))
        if(CONSTANTS.get_sim_debug()):
            draw_sim_attack_model_f2(in_attackModel_F2, "__out_" + get_timestamp())
        del attackTree_i["edges"]

    logger.debug("Simulation results ready in json-f6 form")

    # Dump cosim results to out JSON-F6
    out_filename = CONSTANTS.get_json_dir() + "/" + output_path + "/out_f6" + complexEvents_Ids + "_" + get_timestamp() + ".json"
    if(CONSTANTS.get_no_record()==False and CONSTANTS.get_reduced_record()==False):
        dump_cosimout_f6(out_filename, data_F6)

    # send to http-endpoint
    if (out_endpoint != None):
        try:
            logger.info("Send out result")
            post_cosimout_f6(out_endpoint, data_F6, output_type)
        except Exception as ex:
            logger.info("Error sending JSON: " + str(ex))


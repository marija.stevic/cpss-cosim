import cacheaccess.cache_props as cache_props
import redis
import datetime
import logging
import time
import json
from api.perf_m import timer

cache_conn = None
cache_scanner = None

def create_cache_conn():
    global cache_conn, cache_scanner
    cache_conn = redis.Redis(host=cache_props.get_cache_properties('host'), \
                            port=cache_props.get_cache_properties('port'))
    cache_scanner = redis.StrictRedis(host=cache_props.get_cache_properties('host'), \
                            port=cache_props.get_cache_properties('port'))

logger=logging.getLogger('simulator_logger')

def get_timestamp():
    return str(datetime.datetime.now().time()).replace(':','_')[-5:]

@timer
def set_event(tree_name, event_type, node_name, event_details, event_identifier=None, expiary=None):

    #logger.info("Cache configs: {}".format(str(cache_props.cache_props)))
    if(cache_conn==None):
        create_cache_conn()
        if(cache_conn==None):
            logger.error("Cache realization failed.")

    retries=cache_props.get_cache_properties('max_retry')
    key=None
    try:
        while(retries>0):
            try:
                if event_identifier!=None:
                    event_id = event_identifier
                else:
                    event_id = get_timestamp()
                key = "event_DET_{}_{}_{}".format(tree_name, event_type, event_id)
                cache_conn.set(key, json.dumps(event_details), \
                    ex=cache_props.get_cache_properties('lifetime_in_seconds'))
                key = "event_"+"{}_{}_{}".format(tree_name, event_type, event_id)
                cache_conn.set(key, str(node_name), \
                    ex=cache_props.get_cache_properties('lifetime_in_seconds'))
                break
            except redis.exceptions.ConnectionError as ex:
                logger.error("retrying cache set op.." + str(ex))
                if retries == 0:
                    raise ex
                retries -= 1
                time.sleep(0.5)
    except Exception as ex:
        logger.error("cache set event failed: {}".format(str(ex)))
        raise ex
    return key

@timer
def get_all_relevant_events(tree_name, event_type=None):
    result_nodes = []
    result_events = []
    res = None
    res_events = None
    #logger.info("Cache configs: {}".format(str(cache_props.cache_props)))
    if(cache_scanner==None):
        create_cache_conn()
        if(cache_scanner==None):
            logger.error("Cache realization failed.")

    retries=cache_props.get_cache_properties('max_retry')
    key=tree_name + "_"
    if(event_type!=None):
        key=key + event_type + "_"
    det_key="event_DET_"+key+"F*"
    key="event_"+key+"F*"
    try:
        while(retries>0):
            try:
                if(res==None):
                    res = cache_scanner.keys(pattern=key)
                logger.info("fetched recent events: {}, {}".format(res, retries))
                for k in res:
                    result_nodes.append(str(cache_conn.get(k).decode("utf-8")))
                
                if(res_events==None):
                    res_events = cache_scanner.keys(pattern=det_key)
                for k in res_events:
                    result_events.append(str(cache_conn.get(k).decode("utf-8")))
                #logger.info("fetched recent events: {}, {}, {}".format(det_key, res_events, retries))
                
                break
            except redis.exceptions.ConnectionError as ex:
                logger.error("retrying cache set op.." + str(ex))
                if retries == 0:
                    raise ex
                retries -= 1
                time.sleep(0.5)
    except Exception as ex:
        logger.error("cache set event failed: {}".format(str(ex)))
        raise ex
    return [result_nodes, result_events]

# get any entry
@timer
def get_entry(key):
    #logger.info("Cache configs: {}".format(str(cache_props.cache_props)))
    if(cache_conn==None):
        create_cache_conn()
        if(cache_conn==None):
            logger.error("Cache realization failed.")

    retries=cache_props.get_cache_properties('max_retry')
    value=None
    try:
        while(retries>0):
            try:
                value=cache_conn.get(key)
                break
            except redis.exceptions.ConnectionError as ex:
                logger.error("retrying cache set op.." + str(ex))
                if retries == 0:
                    raise ex
                retries -= 1
                time.sleep(0.5)
    except Exception as ex:
        logger.error("cache set event failed: {}".format(str(ex)))
        raise ex
    return value

def set_entry(k, v):
    #logger.info("Cache configs: {}".format(str(cache_props.cache_props)))
    if(cache_conn==None):
        create_cache_conn()
        if(cache_conn==None):
            logger.error("Cache realization failed.")

    #logger.info("Key: {}, value: {}".format(k, v))
    retries=cache_props.get_cache_properties('max_retry')
    try:
        while(retries>0):
            try:
                cache_conn.set(k, v)
                break
            except redis.exceptions.ConnectionError as ex:
                logger.error("retrying cache set op.." + str(ex))
                if retries == 0:
                    raise ex
                retries -= 1
                time.sleep(0.5)
    except Exception as ex:
        logger.error("cache set event failed: {}".format(str(ex)))
        raise ex
    return

def flush_all_data():
    logger.info("Cache configs: {}".format(str(cache_props.cache_props)))
    if(cache_scanner==None):
        create_cache_conn()
        if(cache_scanner==None):
            logger.error("Cache realization failed.")
    
    if(cache_conn==None):
        create_cache_conn()
        if(cache_conn==None):
            logger.error("Cache realization failed.")

    retries=cache_props.get_cache_properties('max_retry')
    key="event*"
    try:
        while(retries>0):
            try:
                res = cache_scanner.keys(pattern=key)
                for k in res:
                    cache_conn.delete(k)
                
                break
            except redis.exceptions.ConnectionError as ex:
                logger.error("retrying cache set op.." + str(ex))
                if retries == 0:
                    raise ex
                retries -= 1
                time.sleep(0.5)
    except Exception as ex:
        logger.error("cache set event failed: {}".format(str(ex)))
        raise ex
    return 


def test_cache():
    #wait for upto a minute
    for i in range(12):
        try:
            create_cache_conn()
        except Exception as ex:
            logger.error("retrying cache connect")
        if(cache_conn==None):
            time.sleep(5)
        else:
            return True
    
    return False

def set_correlated_events(tree_name, extendedCorrEvents, corrEventProb):
    #logger.info("Cache configs: {}".format(str(cache_props.cache_props)))
    if(cache_conn==None):
        create_cache_conn()
        if(cache_conn==None):
            logger.error("Cache realization failed.")

    #logger.info("Key: {}, value: {}".format(k, v))
    retries=cache_props.get_cache_properties('max_retry')
    logger.info("current correlated events: {}, {}".format(extendedCorrEvents, corrEventProb))
    try:
        while(retries>0):
            try:
                for e in extendedCorrEvents:
                    cache_conn.set("event_" + tree_name + "_corr_event_" + e, str(corrEventProb), \
                            ex=cache_props.get_cache_properties('lifetime_in_seconds'))
                break
            except redis.exceptions.ConnectionError as ex:
                logger.error("retrying cache set op.." + str(ex))
                if retries == 0:
                    raise ex
                retries -= 1
                time.sleep(0.5)
    except Exception as ex:
        logger.error("cache set event failed: {}".format(str(ex)))
        raise ex
    return


def get_all_correlated_events(tree_name, correlated_events, correlated_subgoals):
    if(cache_scanner==None):
        create_cache_conn()
        if(cache_scanner==None):
            logger.error("Cache realization failed.")

    retries=cache_props.get_cache_properties('max_retry')
    key="event_" + tree_name + "_corr_event_*"
    node_id_start_index=len(key)-1
    try:
        while(retries>0):
            try:
                res = cache_scanner.keys(pattern=key)
                for k in res:
                    prob = float(str(cache_conn.get(k).decode("utf-8")))
                    node_name = str(k[node_id_start_index:].decode("utf-8"))
                    if prob < 0.999:
                        correlated_events[node_name]=prob
                    else:
                        correlated_subgoals.append(node_name)
                logger.info("fetched correleted events: {}".format(correlated_events))

                break
            except redis.exceptions.ConnectionError as ex:
                logger.error("retrying cache set op.." + str(ex))
                if retries == 0:
                    raise ex
                retries -= 1
                time.sleep(0.5)
    except Exception as ex:
        logger.error("cache set event failed: {}".format(str(ex)))
        raise ex

    return


cache_props=dict()

def set_cache_properties(key, value, override=True):
    global cache_props
    if override:
        cache_props[key] = value
    elif(key not in cache_props):
        cache_props[key] = value
    if(key!='host'):
        cache_props[key] = int(cache_props[key])

def get_cache_properties(key):
    return cache_props[key]

def get_cache_props():
    return cache_props
    
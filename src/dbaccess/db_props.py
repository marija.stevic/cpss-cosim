db_props=dict()

def set_db_properties(key, value, override=True):
    global db_props
    if override:
        db_props[key] = value
    elif key not in db_props:
        db_props[key] = value

def get_db_properties(key):
    return db_props[key]
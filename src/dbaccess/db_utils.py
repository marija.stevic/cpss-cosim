import dbaccess.db_props as db_props
import mysql.connector
import logging
import json

deactivate_db=True

CHECK_TREE_EXISTS = "SELECT COUNT(1) FROM COSIM_ATTACK_TREES where TREE_NAME='{}'"

INSERT_TREE = "INSERT INTO COSIM_ATTACK_TREES (TREE_NAME, ROOT, SUBGOALS, LEAF_NODES, EDGES, TRANSITIONS) " +\
                        "VALUES(\"{}\", \"{}\", \"{}\", \"{}\", \"{}\", \"{}\")"

GET_TREE_BY_NAME = "SELECT * FROM COSIM_ATTACK_TREES where TREE_NAME='{}'"

INSERT_COMPROMISE_EVENT = "INSERT into COSIM_EVENTS_COMPROMISE (EVENT_IDENTIFIER, TREE_NAME, " + \
                                "START_TIME, END_TIME, DETAILS) VALUES (\"{}\", \"{}\", \"{}\", \"{}\", \"{}\")"
GET_COMPROMISE_EVENT = "SELECT * from COSIM_EVENTS_COMPROMISE where ID={}"

INSERT_COUNTERMEASURE_EVENT = "INSERT into COSIM_EVENTS_COUNTERMEASURE (EVENT_IDENTIFIER, TREE_NAME, " + \
                                "START_TIME, END_TIME, DETAILS) VALUES (\"{}\", \"{}\", \"{}\", \"{}\", \"{}\")"
GET_COUNTERMEASURE_EVENT = "SELECT * from COSIM_EVENTS_COUNTERMEASURE where ID={}"

INSERT_SIMULATION_RESULT = "INSERT into COSIM_SIM_RESULT (EVENT_ID, EVENT_TYPE, TREE_NAME, " + \
                                "NODE_ID, PROBABILITIES) VALUES ({}, \"{}\", \"{}\", \"{}\", \"{}\")"

GET_SIMULATION_RESULT_BY_EVENT_ID_AND_TYPE = "SELECT NODE_ID, PROBABILITIES from COSIM_SIM_RESULT " +\
                            "where EVENT_ID='{}' and EVENT_TYPE='{}'"

#most recent result for tree
GET_LAST_SIMULATION_RESULT_EVENT_ID_BY_TREE_TYPE = "SELECT EVENT_ID, EVENT_TYPE from " + \
                                "COSIM_SIM_RESULT where ID=(SELECT ID from COSIM_SIM_RESULT where " +\
                                "TREE_NAME='{}' ORDER BY ID DESC limit 1)"

GET_LAST_SIMULATION_RESULT_EVENT_ID_BY_TREE_TYPE_AND_EVENT_TYPE = "SELECT EVENT_ID from " + \
                            "COSIM_SIM_RESULT where ID=(SELECT ID FROM COSIM_SIM_RESULT TREE_NAME='{}' " + \
                            "AND EVENT_TYPE='{}' ORDER BY ID DESC limit 1)"

logger=logging.getLogger('simulator_logger')

config = None

def get_config():

    global config
    logger.debug("getting config")
    if(config!=None):
        return config

    config = dict()
    config['user'] = db_props.get_db_properties('username')
    config['password'] = db_props.get_db_properties('password')
    config['host'] = db_props.get_db_properties('host')
    config['port'] = db_props.get_db_properties('port')
    config['database'] = db_props.get_db_properties('database')

    logger.debug("configs fetched: " + str(config))
    return config

def execute_query(query, args=None):
    results = []
    connection = None
    try:
        #logger.debug("creating conn")
        if(config==None):
            get_config()
        connection = mysql.connector.connect(**config)
        cursor = connection.cursor()
        #logger.debug("exec statement")
        if(args!=None):
            logger.debug("query formatted: " + query.format(*args)[:50])
            cursor.execute(query.format(*args))
        else:
            cursor.execute(query)
        #logger.debug("execed statement")
        results = cursor.fetchall()
        cursor.close()
    except Exception as ex:
        logger.error(ex)
        raise ex
    finally:
        if(connection!=None):
            connection.close()
    return results

def execute_insert_query(query, args=None):
    rowid = -1
    connection = None
    try:
        #logger.debug("creating conn")
        if(config==None):
            get_config()
        connection = mysql.connector.connect(**config)
        cursor = connection.cursor()
        #logger.debug("exec statement")
        if(args!=None):
            logger.debug("query formatted: " + query.format(*args)[:150])
            cursor.execute(query.format(*args))
        else:
            cursor.execute(query)
        logger.debug("execed statement " + str(cursor.lastrowid))
        rowid = cursor.lastrowid
        connection.commit()
        cursor.close()
    except Exception as ex:
        logger.error(ex)
        raise ex
    finally:
        if(connection!=None):
            connection.close()
    return rowid

def check_tree_exists(treename):
    res = False
    connection = None
    try:
        logger.debug("creating conn")
        if(config==None):
            get_config()
        connection = mysql.connector.connect(**config)
        cursor = connection.cursor()
        query = CHECK_TREE_EXISTS.format(treename)
        logger.info("query formatted: " + query)
        cursor.execute(query)
        logger.debug("execed statement")
        res = cursor.fetchone()[0]
        logger.debug("result: " + str(res))
        cursor.close()
    except Exception as ex:
        logger.error(ex)
        raise ex
    finally:
        if(connection!=None):
            connection.close()
    return res

def insert_tree(treename, root, subgoals, leaf_nodes, edges, tranistions):
    logger.info("inserting tree in database")
    result = execute_insert_query(INSERT_TREE, [str(treename), str(root), str(subgoals), str(leaf_nodes),\
                                         str(edges), str(tranistions)])
    logger.info("inserted tree with id {}".format(result))

def get_tree_by_name(treename):
    logger.info("getting tree from db: " + str(treename))
    result = execute_query(GET_TREE_BY_NAME, ([treename]))[0]
    logger.info("fetched tree details: id:{}, name:{}, root:{}".format(result[0], result[1], result[2]))
    return result

def insert_compromise_event(treename, request_body):
    if(deactivate_db):
        return 1
    logger.info("executing insert compromise event query.")
    id = None
    startTime = None
    endTime = None
    if('id' in request_body):
        id = str(request_body['id'])
    if('startTime' in request_body):
        startTime = str(request_body['startTime'])
    if('endTime' in request_body):
        endTime = str(request_body['endTime'])
    logger.info(str(id))
    result = execute_insert_query(INSERT_COMPROMISE_EVENT, [id, treename, startTime, endTime, str(request_body)])
    logger.info("inserted compromise event with id {}".format(result))
    return result

def get_compromise_event(event_id):
    result = execute_query(GET_COMPROMISE_EVENT, (event_id))[0]
    logger.info("fetched compromise event with id {} with details {}".format(result[0], str(result[-1])[:25]))
    return result

def insert_countermeasure_event(treename, request_body):
    if(deactivate_db):
        return 1
    logger.info("executing insert countermeasure event query.")
    id = None
    startTime = None
    endTime = None
    if('id' in request_body):
        id = str(request_body['id'])
    if('startTime' in request_body):
        startTime = str(request_body['startTime'])
    if('endTime' in request_body):
        endTime = str(request_body['endTime'])
    result = execute_insert_query(INSERT_COUNTERMEASURE_EVENT, [id, treename, startTime, endTime, str(request_body)])
    logger.info("inserted countermeasure event with id {}".format(result))
    return result

def get_countermeasure_event(event_id):
    result = execute_query(GET_COUNTERMEASURE_EVENT, (event_id))[0]
    logger.debug("fetched countermeasure event with id {} with details {}".format(result[0], str(result[-1])[:25]))
    return result

def insert_simulation_result(sim_result, event_id, event_type, treename):
    if(deactivate_db):
        return 1
    logger.info("simulation result populating to table: " + str(sim_result)[:25])
    for k in sim_result.keys():
        res = execute_insert_query(INSERT_SIMULATION_RESULT, [event_id, event_type, treename, \
                                            k, str(sim_result[k])])
    # evict simulation result entry from cache
    # populate event id in cache in "event_type + event_id"
    logger.info("populated simulation result to db for event id: " + str(event_id))
    return

def get_simulation_result(treename, event_type=None, event_id=None):
    logger.info("getting simulation result for: " + str(treename))
    if(event_type==None):
        [event_id, event_type] = execute_query(GET_LAST_SIMULATION_RESULT_EVENT_ID_BY_TREE_TYPE, [treename])[0]
    else:
        event_id = execute_query(GET_LAST_SIMULATION_RESULT_EVENT_ID_BY_TREE_TYPE_AND_EVENT_TYPE, \
                                [treename, event_type])[0]

    result=dict()
    fetched_rows=execute_query(GET_SIMULATION_RESULT_BY_EVENT_ID_AND_TYPE, [event_id, event_type])
    for row in fetched_rows:
        [node_id, probabilities] = row
        logger.debug("received sim result entry for node: {}, as {}".format(node_id, probabilities[:25]))
        result[node_id] = json.loads(probabilities)
    
    logger.info("returning simulation result..")
    return result

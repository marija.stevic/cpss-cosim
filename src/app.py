import json
import snakes.plugins
snakes.plugins.load("gv", "snakes.nets", "nets")    # line has to remain here
from nets import *
from flask import Flask, Blueprint

from ctmcsim.ctmc_analyser import simulate

from parsers.jsonparser_AttackModel_F2 import *
from parsers.jsonparser_in_F7 import *
from parsers.jsonparser_in_F8 import *
from parsers.jsonparser_out_F6 import *
from parsers.jsonparser_ctmc import *

from api.constants import initialize_cmd_line_props as mod_cmd
from api.constants import set_new_norm, set_sim_debug, set_no_record, set_reduced_record, set_append_correlated_events

import cacheaccess.cache_props as cache_props
import cacheaccess.cache_utils as cache_utils

import sys
import re

config = {}       # config variables
no_simulations=0

APP_NAME='cosimulator__'

app=Flask(__name__)
import api.api as api

def setup_logger():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

def parse_bool(s):
    if s in ['False', 'false', 'F'] :
        return False
    return True

def configure_api_properties(filename):

    # populate default values
    api.props_f7 = dict()
    api.props_f7['input_folder'] = None
    api.props_f7['out_endpoint'] = None
    api.props_f7['deactivate_endpoint'] = False
    api.props_f7['out_response_type'] = 'json'
    api.props_f7['output_path'] = 'output_files/'

    api.props_f8 = dict()
    api.props_f8['input_folder'] = None
    api.props_f8['out_endpoint'] = None
    api.props_f8['deactivate_endpoint'] = False
    api.props_f8['out_response_type'] = 'json'
    api.props_f8['output_path'] = 'output_files/'

    #read environment variables
    if(APP_NAME+"sim_debug" in os.environ):
        set_sim_debug(parse_bool(os.environ[APP_NAME+"sim_debug"]))

    if(APP_NAME+"reduced_record" in os.environ):
        set_reduced_record(parse_bool(os.environ[APP_NAME+"reduced_record"]))
    
    if(APP_NAME+"no_record" in os.environ):
        set_no_record(parse_bool(os.environ[APP_NAME+"no_record"]))
    
    if(APP_NAME+"alternate_norm" in os.environ):
        set_new_norm(parse_bool(os.environ[APP_NAME+"alternate_norm"]))

    if(APP_NAME+"append_correlated_events" in os.environ):
        set_append_correlated_events(parse_bool(os.environ[APP_NAME+"append_correlated_events"]))

    prop_prefix = APP_NAME + "props_f7__"
    if (prop_prefix + "in_foldername" in os.environ):
        api.props_f7['input_folder'] = os.environ[prop_prefix + "in_foldername"]

    if (prop_prefix + "out_endpoint" in os.environ):
        api.props_f7['out_endpoint'] = os.environ[prop_prefix + "out_endpoint"]

    if (prop_prefix + "deactivate_endpoint" in os.environ):
        api.props_f7['deactivate_endpoint'] = os.environ[prop_prefix + "deactivate_endpoint"]

    if (prop_prefix + "out_response_type" in os.environ):
        api.props_f7['out_response_type'] = os.environ[prop_prefix + "out_response_type"]

    if (prop_prefix + "output_path" in os.environ):
        api.props_f7['output_path'] = os.environ[prop_prefix + "output_path"]

    prop_prefix = APP_NAME + "props_f8__"
    logging.info(prop_prefix)
    if (prop_prefix + "in_foldername" in os.environ):
        api.props_f8['input_folder'] = os.environ[prop_prefix + "in_foldername"]

    if (prop_prefix + "out_endpoint" in os.environ):
        api.props_f8['out_endpoint'] = os.environ[prop_prefix + "out_endpoint"]

    if (prop_prefix + "deactivate_endpoint" in os.environ):
        api.props_f8['deactivate_endpoint'] = os.environ[prop_prefix + "deactivate_endpoint"]

    if (prop_prefix + "out_response_type" in os.environ):
        api.props_f8['out_response_type'] = os.environ[prop_prefix + "out_response_type"]

    if (prop_prefix + "output_path" in os.environ):
        api.props_f8['output_path'] = os.environ[prop_prefix + "output_path"]

    if (prop_prefix + "test_f8" in os.environ):
        api.props_f8['test_f8'] = os.environ[prop_prefix + "test_f8"]

    prop_prefix = APP_NAME + "cache_"
    props_cache = ['host', 'port', 'lifetime_in_seconds', 'max_retry']
    for prop in props_cache:
        if(prop_prefix + prop in os.environ):
            cache_props.set_cache_properties(prop, os.environ[prop_prefix + prop])

    #read local config to read unpopulated values
    with open(filename) as file:
        config = json.load(file)

    if("sim_debug" in config):
        set_sim_debug(config['sim_debug'])

    if ("props_f7" in config):

        temp = config["props_f7"]
        if ("in_foldername" in temp and api.props_f7['input_folder']==None):
            api.props_f7['input_folder'] = temp["in_foldername"]

        if ("out_endpoint" in temp and api.props_f7['out_endpoint']==None):
            api.props_f7['out_endpoint'] = temp["out_endpoint"]

        if ("deactivate_endpoint" in temp and api.props_f7['deactivate_endpoint']==False):
            api.props_f7['deactivate_endpoint'] = temp["deactivate_endpoint"]

        if ("out_response_type" in temp and api.props_f7['out_response_type']=='json'):
            api.props_f7['out_response_type'] = temp["out_response_type"]

        if ("output_path" in temp and api.props_f7['output_path']=='output_files/'):
            api.props_f7['output_path'] = temp["output_path"]

    if ("props_f8" in config):

        temp = config["props_f8"]

        if ("in_foldername" in temp and api.props_f8['input_folder'] == None):
            api.props_f8['input_folder'] = temp["in_foldername"]

        if ("out_endpoint" in temp and api.props_f8['out_endpoint'] == None):
            api.props_f8['out_endpoint'] = temp["out_endpoint"]

        if ("deactivate_endpoint" in temp and api.props_f8['deactivate_endpoint'] == False):
            api.props_f8['deactivate_endpoint'] = temp["deactivate_endpoint"]

        if ("out_response_type" in temp and api.props_f8['out_response_type'] == 'json'):
            api.props_f8['out_response_type'] = temp["out_response_type"]

        if ("output_path" in temp and api.props_f8['output_path'] == 'output_files/'):
            api.props_f8['output_path'] = temp["output_path"]

    if("cache" in config):
        for prop in props_cache:
            if(prop_prefix + prop in os.environ):
                cache_props.set_cache_properties(prop, os.environ[prop_prefix + prop], False)



def get_json_ip_files(folder_name):

    valid_files = []
    invalid_files = []

    files = {}

    for r, d, f in os.walk(folder_name + "/valid"):
        for file in f:
            if '.json' in file:
                valid_files.append(os.path.join(r, file))

    for r, d, f in os.walk(folder_name + "/invalid"):
        for file in f:
            if '.json' in file:
                invalid_files.append(os.path.join(r, file))

    def atoi(text):
        if text.isdigit():
            return int(text) 
        else:
            return text    
    def natural_keys(text):
        return [ atoi(c) for c in re.split('(\d+)',text) ]
    valid_files.sort(key=natural_keys)
    print(valid_files)
    logger.info("valid_files: " + str(valid_files))

    invalid_files.sort(key=natural_keys)

    files['valid'] = valid_files
    files['invalid'] = invalid_files

    return files

def simulate_test_event(in_attackModel_F2, ctmc_models, filename, expected_exception):

    print("Simulating events for " + filename)
    in_attackModel_F2_copy = deepcopy(in_attackModel_F2)
    all_events = None
    tree_name = ''

    try:
        [in_attackModel_F2_copy, tree_name, node_name, recentEventDetails, all_events, correlated_events, req_json] = \
                                parse_events_f7(filename, in_attackModel_F2_copy, ctmc_models)
    except Exception as e:
        print("Error: co-simulator did not receive an event associated "
              "to the attack tree nodes in {}!".format(filename))
        return True == expected_exception

    cache_key=None
    if('id' in req_json):
        cache_key=cache_utils.set_event(tree_name, "F7", node_name, req_json, req_json['id'])
    else:
        cache_key=cache_utils.set_event(tree_name, "F7", node_name, req_json)
    logger.info("set input event in cache with key: " + str(cache_key))

    #print([in_attackModel_F2_copy, all_events, req_json, tree_name])
    try:
        initial_state_index=cache_utils.get_entry(\
                            tree_name+"_"+in_attackModel_F2_copy["attackModel"][0]["current_aggregated_state"])
        #print(initial_state_index)
        [individual_transition_rates, modified_leaf_probabilities] = create(ctmc_models[tree_name]["optimized_transition_lists"], \
                                ctmc_models[tree_name]["leaf_probabilities"], all_events, ctmc_models[tree_name]["place_list"], correlated_events)
        #print(state_transition_matrix)
    except Exception as ex:
        print("Error in creating PN model companion in {}: {}".format(filename, str(ex)))
        return True == expected_exception

    try:
        simulate(in_attackModel_F2_copy, 'json', None, api.props_f7['output_path'], 'f7', tree_name, \
                        req_json, recentEventDetails, initial_state_index, \
                        individual_transition_rates, modified_leaf_probabilities, \
                        ctmc_models[tree_name]["state_transition_matrix"]["to_indexes"], \
                        ctmc_models[tree_name]["state_transition_matrix"]["from_indexes"], \
                        ctmc_models[tree_name]["state_transition_matrix"]["transition_indexes"], \
                        ctmc_models[tree_name]["state_transition_matrix"]["transition_indexes_len"], \
                        ctmc_models[tree_name]["no_ctmc_states"], ctmc_models[tree_name]["place_list"],
                        ctmc_models[tree_name]["aggregate_state_labels"])
    except Exception as ex:
        print("Error in simulation: " + str(ex))
        return True == expected_exception

    # cache_utils.flush_all_data()
    return False == expected_exception

def simulate_test_event_f8(in_attackModel_F2, ctmc_models, filename, expected_exception):

    print("Simulating events for " + filename)
    in_attackModel_F2_copy = deepcopy(in_attackModel_F2)
    all_events = None
    tree_name = ''
    mitigation_model = None

    try:
        [in_attackModel_F2_copy, all_events, req_json, tree_name, correlated_events, mitigation_model] = \
                                parse_events_f8(filename, in_attackModel_F2_copy, ctmc_models)
    except Exception as e:
        print("Error: exception: {}".format(e))
        print("Error: co-simulator did not receive an event associated "
              "to the attack tree nodes in {}!".format(filename))
        return True == expected_exception

    #print([in_attackModel_F2_copy, all_events, req_json, tree_name])
    try:
        initial_state_index=cache_utils.get_entry(\
                            tree_name+"_"+in_attackModel_F2_copy["attackModel"][0]["current_aggregated_state"])
        #print(initial_state_index)
        [individual_transition_rates, modified_leaf_probabilities] = create(ctmc_models[tree_name]["optimized_transition_lists"], \
                                ctmc_models[tree_name]["leaf_probabilities"], all_events, ctmc_models[tree_name]["place_list"], correlated_events)
        #print(state_transition_matrix)
    except Exception as ex:
        print("Error in creating PN model companion in {}: {}".format(filename, str(ex)))
        return True == expected_exception

    try:
        simulate(in_attackModel_F2_copy, 'string', None, api.props_f8['output_path'], 'f8', '', \
                        req_json, None, initial_state_index, \
                        individual_transition_rates, modified_leaf_probabilities, \
                        ctmc_models[tree_name]["state_transition_matrix"]["to_indexes"], \
                        ctmc_models[tree_name]["state_transition_matrix"]["from_indexes"], \
                        ctmc_models[tree_name]["state_transition_matrix"]["transition_indexes"], \
                        ctmc_models[tree_name]["state_transition_matrix"]["transition_indexes_len"], \
                        ctmc_models[tree_name]["no_ctmc_states"], ctmc_models[tree_name]["place_list"],
                        ctmc_models[tree_name]["aggregate_state_labels"], mitigation_model)
    except Exception as ex:
        print("Error in simulation: " + str(ex))
        return True == expected_exception

    return False == expected_exception


def main_call(is_server_proc):
    print("Cyber-Physical-Social-System Co-simulator started")

    try:
        configure_api_properties(CONSTANTS.get_json_dir() + "/configs/config.json")
        setup_logger()
    except Exception as ex:
        print( "Error loading config: " + str(ex) )

    #attack_tree_mdl_filename = JSON_DIR + "/attack_tree_models/JSON_Fmodel2_AttackPaths_v10_test.json"
    attack_mdl_filename = CONSTANTS.get_json_dir() + "/attack_tree_models/AttackModel_merged.json"

    # load json file with attack tree
    in_attackModel_F2 = parse_attack_model_f2(attack_mdl_filename)

    ctmc_models = load_ctmc_models(in_attackModel_F2["attackModel"])
    #print(ctmc_models)

    global no_simulations
    failed_test = 0

    if (api.props_f7['input_folder'] != None):
        print("Simulating f7 event")

        if is_server_proc:
            files = get_json_ip_files("../" + api.props_f7['input_folder'])
        else:
            files = get_json_ip_files(api.props_f7['input_folder'])

        for file in files['valid']:
            check = simulate_test_event(in_attackModel_F2, ctmc_models, file, False)
            if check == False:
                print("!!!!!!!!!!!Input file positive test failed: {}!!!!!!!!!!!!".format(file))
                logger.error("!!!!!!!!!!!Input file positive test failed: {}!!!!!!!!!!!!".format(file))
                failed_test+=1

        for file in files['invalid']:
            check = simulate_test_event(in_attackModel_F2, ctmc_models, file, True)
            if check == False:
                print("!!!!!!!!!!!Input file negative test failed: {}!!!!!!!!!!!!".format(file))
                logger.error("!!!!!!!!!!!Input file negative test failed: {}!!!!!!!!!!!!".format(file))
                failed_test+=1

        no_simulations = no_simulations + 1  # expected command line simulation only

    if (api.props_f8['input_folder'] != None):
        print("Simulating f8 event")

        if is_server_proc:
            files = get_json_ip_files("../" + api.props_f8['input_folder'])
        else:
            files = get_json_ip_files(api.props_f8['input_folder'])
        print(files)
        for file in files['valid']:
            check = simulate_test_event_f8(in_attackModel_F2, ctmc_models, file, False)
            if check==False:
                print("!!!!!!!!!!!Input file positive test failed: {}!!!!!!!!!!!!".format(file))
                logger.error("!!!!!!!!!!!Input file positive test failed: {}!!!!!!!!!!!!".format(file))
                failed_test+=1

        for file in files['invalid']:
            check = simulate_test_event_f8(in_attackModel_F2, ctmc_models, file, True)
            if check==False:
                print("!!!!!!!!!!!Input file negative test failed: {}!!!!!!!!!!!!".format(file))
                logger.error("!!!!!!!!!!!Input file negative test failed: {}!!!!!!!!!!!!".format(file))
                failed_test+=1

        no_simulations = no_simulations + 1  # expected command line simulation only

    if(no_simulations==0 or is_server_proc):
        # Start API-Server waiting for JSON-input
        api.attackModel = deepcopy(in_attackModel_F2) # set the api-attack-model
        api.ctmcModel = ctmc_models

    cache_utils.flush_all_data()
    print("all simulation ended")
    logger.info("failed test: " + str(failed_test))


if __name__ == "__main__":
    #setup_logger()
    mod_cmd()
    main_call(False)
    if (no_simulations == 0):
        app.register_blueprint(api.api)
        app.config['ran_from_command_line']=True
        app.run("0.0.0.0", 8080, debug=True, use_reloader=False, threaded=True)  # processes=3


    


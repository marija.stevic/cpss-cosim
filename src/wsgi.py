from app import app, main_call
from api import api
from flask import Flask

main_call(True)
app = Flask(__name__)
app.register_blueprint(api.api)

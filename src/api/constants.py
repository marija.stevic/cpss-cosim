JSON_DIR_CMD = "./json_files"
FIGURE_DIR_CMD = "./figures"
DEFAULT_LOG_FILE_CMD ='./simulator_api.log'

JSON_DIR = "../json_files"
FIGURE_DIR = "../figures"
DEFAULT_LOG_FILE ='../simulator_api.log'

SIM_DEBUG = False
REDUCED_RECORD = True
NO_RECORD = False
APPEND_CORR_EVENTS = False
NEW_NORM = False

def initialize_cmd_line_props():
    global JSON_DIR
    global FIGURE_DIR
    global DEFAULT_LOG_FILE

    JSON_DIR = JSON_DIR_CMD
    FIGURE_DIR = FIGURE_DIR_CMD
    DEFAULT_LOG_FILE = DEFAULT_LOG_FILE_CMD

def get_json_dir():
    return JSON_DIR

def get_figure_dir():
    return FIGURE_DIR

def get_log_file_name():
    return DEFAULT_LOG_FILE

def set_sim_debug(val):
    global SIM_DEBUG
    if SIM_DEBUG:
        return
    SIM_DEBUG = val

def get_sim_debug():
    return SIM_DEBUG

def set_reduced_record(val):
    global REDUCED_RECORD
    REDUCED_RECORD = val

def get_reduced_record():
    return REDUCED_RECORD

def set_no_record(val):
    global NO_RECORD
    NO_RECORD = val

def get_no_record():
    return NO_RECORD

def set_append_correlated_events(val):
    global APPEND_CORR_EVENTS
    APPEND_CORR_EVENTS = val

def get_append_correlated_events():
    return APPEND_CORR_EVENTS

def set_new_norm(val):
    global NEW_NORM
    NEW_NORM = val

def get_new_norm():
    return NEW_NORM

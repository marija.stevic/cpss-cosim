from flask import Blueprint, request, Response, current_app, json
from flask_cors import CORS
from werkzeug.exceptions import BadRequest
import json
from threading import Thread, local, currentThread

import logging
import logging.handlers

from parsers.jsonparser_in_F7 import parse_obj_f7
from parsers.jsonparser_in_F8 import parse_obj_f8
from api.perf_m import timer

import pnsim.pn_basic_sim as pnsim
import ctmcsim.ctmc_analyser as ctmc_analyser
import dbaccess.db_utils as db_utils
import cacheaccess.cache_utils as cache_utils

import api.constants as CONSTANTS
from parsers.jsonparser_AttackModel_F2 import *

JSON_MIME = "application/json; charset=utf-8"

attackModel = None
ctmcModel = None

props_f7=None
props_f8=None

api = Blueprint("api", __name__, url_prefix="/api")
CORS(api)

logger=None
log_config=None

api._got_first_request = False

api_type = local()
api_type.req_type = ''

class API_type_filter(logging.Filter):
    def filter(self, record):
        if (currentThread().getName() == 'MainThread') \
                or (not hasattr(api_type, 'req_type')) \
                or api_type.req_type == '':
            record.api_type = request.url_rule.rule[-2:]
            api_type.req_type = request.url_rule.rule[-2:]
        else:
            record.api_type = api_type.req_type
        return True

def setup_logger():

    if api._got_first_request==True:
        return

    global logger
    global log_config
    logger = logging.getLogger('simulator_logger')
    logger.addFilter(API_type_filter())
    logger.setLevel(logging.DEBUG)
    logging_levels=\
        {'INFO':logging.INFO,'DEBUG':logging.DEBUG,'ERROR':logging.ERROR}

    if log_config==None:
        log_config={}
        try:
            with open(CONSTANTS.JSON_DIR+"/configs/log_config.json") as file:
                print("configuring log props")
                log_config=json.load(file)
                print(log_config)
        except Exception as e:
            print("no logging configuration found. using default settings.")

    max_log_size = 250 * 1024 * 1024  # 250MB max log size default
    if ("max_log_size" in log_config):
       max_log_size = log_config['max_log_size'] * 1024 * 1024

    max_old_logs = 5  # max no of old log files
    if ("max_old_logs" in log_config):
        max_old_logs = log_config['max_old_logs']

    log_level = 'INFO'  # default log level
    if ("log_level" in log_config):
       log_level = log_config['log_level']

    console_level = 'INFO'  # default print level
    if ("console_level" in console_level):
       console_level = log_config['console_level']

    #absolute path to log file
    log_file_name=CONSTANTS.get_log_file_name()
    if("log_file_name" in log_config):
        log_file_name = get_adjusted_file_name(log_config['log_file_name'])

    flog=logging.handlers.RotatingFileHandler(log_file_name,
              mode='a', maxBytes=max_log_size, backupCount=max_old_logs)

    log_format = "[%(levelname)s] [%(asctime)s] [%(process)d] [%(threadName)s] [%(api_type)s]: %(message)s"
    if("log_format" in log_config):
        log_format = log_config["log_format"]

    formatter = logging.Formatter(log_format)

    flog.setFormatter(formatter)
    flog.setLevel(logging_levels[log_level])
    logger.addHandler(flog)

    if("enable_console_log" in log_config and log_config['enable_console_log']):
        clog=logging.StreamHandler()
        clog.setLevel(logging_levels[console_level])
        clog.setFormatter(formatter)
        logger.addHandler(clog)

    current_app.logger=logger
    api._got_first_request = True
    logger.info("logger configured")

api.before_request(setup_logger)

def call_simulator(in_attackModel_F2, output_type, out_endpoint, endpoint, \
                            output_path, treename, request_body, recentEventDetails):
    api_type.req_type = endpoint
    pnsim.simulate(in_attackModel_F2, output_type, out_endpoint, output_path, \
                            endpoint, treename, request_body, recentEventDetails)

def call_simulator_ctmc(in_attackModel_F2, output_type, out_endpoint, output_path, \
                            endpoint, treename, request_body, recentEventDetails, initial_state_index,\
                                individual_transition_rates, modified_leaf_probabilities, to_indexes, from_indexes, transition_indexes, \
                                transition_indexes_len, no_states, place_list, aggregate_state_labels, mitigation_model=None):
    api_type.req_type = endpoint
    ctmc_analyser.simulate(in_attackModel_F2, output_type, out_endpoint, output_path, \
                            endpoint, treename, request_body, recentEventDetails, initial_state_index,\
                                individual_transition_rates, modified_leaf_probabilities, to_indexes, from_indexes, transition_indexes, \
                                    transition_indexes_len, no_states, place_list, aggregate_state_labels, mitigation_model)

@api.route("")
def index():
    return Response("OK", 200, mimetype="text/plain")

@timer
@api.route("/f7", methods=["POST", "PUT"])
def post_f7():
    logger.info("Simulating f7 events")

    #logger.info("f7 properties: " + str(props_f7))

    if (props_f7['deactivate_endpoint']==True):
        logger.info("f7 interface deactivated")
        return Response("Invalid request", 400)

    try:
        json_obj = request.get_json(force=True)  # force JSON-parse, exception on error
        [model, tree_name, node_name, recentEventDetails, all_events, correlated_events] = \
                                parse_obj_f7(json_obj, attackModel, ctmcModel)
        json_obj['node_ID'] = node_name
        
        cache_key=None
        if('id' in json_obj):
            cache_key=cache_utils.set_event(tree_name, "F7", node_name, json_obj, json_obj['id'])
        else:
            cache_key=cache_utils.set_event(tree_name, "F7", node_name, json_obj)
        logger.info("set input event in cache with key: " + str(cache_key))

        if not model["attackModel"]:
            logger.error("Error: co-simulator did not receive an event associated to the attack tree nodes!")
        else:
            logger.debug("Received input events")

            initial_state_index=cache_utils.get_entry(\
                            tree_name+"_"+model["attackModel"][0]["current_aggregated_state"])
            logger.info("initial_state_index:{}".format(int(initial_state_index)))

            try:
                [individual_transition_rates, modified_leaf_probabilities] = create(ctmcModel[tree_name]["optimized_transition_lists"], \
                                ctmcModel[tree_name]["leaf_probabilities"], all_events,  ctmcModel[tree_name]["place_list"], correlated_events)
            except Exception as ex:
                logger.error("Error in creating PN model companion: " + str(ex))
                return Response("Error in creating PN model companion: : " + str(ex), 400)

            try:
                # start simulation in separate thread
                t = Thread(target=call_simulator_ctmc, daemon=True, args=[model, \
                        props_f7['out_response_type'], props_f7['out_endpoint'],  \
                        props_f7['output_path'], 'f7', tree_name, json_obj, recentEventDetails, \
                        initial_state_index,  individual_transition_rates, \
                        modified_leaf_probabilities, \
                        ctmcModel[tree_name]["state_transition_matrix"]["to_indexes"], \
                        ctmcModel[tree_name]["state_transition_matrix"]["from_indexes"], \
                        ctmcModel[tree_name]["state_transition_matrix"]["transition_indexes"], \
                        ctmcModel[tree_name]["state_transition_matrix"]["transition_indexes_len"], \
                        ctmcModel[tree_name]["no_ctmc_states"], ctmcModel[tree_name]["place_list"], \
                        ctmcModel[tree_name]["aggregate_state_labels"]])
                t.start()
            except Exception as ex:
                logger.error("Error in simulation: " + str(ex))
                return Response("Error in simulation: " + str(ex), 400)
    except Exception as ex:
        msg = "Error parsing request"
        logger.error(msg + ": " + str(ex))
        return Response(msg + ": " + str(ex), 400)

    return Response("OK", 202)   # 202: accepted, processing

@api.route("/f8", methods=["POST", "PUT"])
def post_f8():
    logger.info("Simulating f8 events")

    #logger.info("f8 properties: " + str(props_f8))

    if (props_f8['deactivate_endpoint']==True):
        logger.info("f8 interface deactivated")
        return Response("Invalid request", 400)

    try:
        json_obj = request.get_json(force=True)
        [model, tree_name, node_name, recentEventDetails, all_events, correlated_events, mitigation_model] = \
                                parse_obj_f8(json_obj, attackModel, ctmcModel)
        json_obj['node_ID'] = node_name
        logger.debug("Received input events")

        if not model["attackModel"]:
            logger.error("Error: co-simulator did not receive an event associated to the attack tree nodes!")
        else:
            logger.debug("Received input events")

            initial_state_index=cache_utils.get_entry(\
                            tree_name+"_"+model["attackModel"][0]["current_aggregated_state"])
            logger.info("initial_state_index:{}".format(int(initial_state_index)))

            try:
                [individual_transition_rates, modified_leaf_probabilities] = create(ctmcModel[tree_name]["optimized_transition_lists"], \
                                ctmcModel[tree_name]["leaf_probabilities"], all_events, ctmcModel[tree_name]["place_list"], correlated_events)
            except Exception as ex:
                logger.error("Error in creating PN model companion: " + str(ex))
                return Response("Error in creating PN model companion: : " + str(ex), 400)

            try:
                # start simulation in separate thread
                t = Thread(target=call_simulator_ctmc, daemon=True, args=[model, \
                        props_f8['out_response_type'], props_f8['out_endpoint'], \
                        props_f8['output_path'], 'f8', tree_name, json_obj, recentEventDetails, \
                        initial_state_index,  individual_transition_rates, \
                        modified_leaf_probabilities, \
                        ctmcModel[tree_name]["state_transition_matrix"]["to_indexes"], \
                        ctmcModel[tree_name]["state_transition_matrix"]["from_indexes"], \
                        ctmcModel[tree_name]["state_transition_matrix"]["transition_indexes"], \
                        ctmcModel[tree_name]["state_transition_matrix"]["transition_indexes_len"], \
                        ctmcModel[tree_name]["no_ctmc_states"], ctmcModel[tree_name]["place_list"], \
                        ctmcModel[tree_name]["aggregate_state_labels"], mitigation_model])
                t.start()
            except Exception as ex:
                logger.error("Error in simulation: " + str(ex))
                return Response("Error in simulation: " + str(ex), 400)
    except Exception as ex:
        msg = "Error parsing request"
        logger.error(msg + ": " + str(ex))
        return Response(msg + ": " + str(ex), 400)

    return Response("OK", 202)   # 202: accepted, processing

def get_adjusted_file_name(file_name):
    if('ran_from_command_line' in current_app.config and
            current_app.config['ran_from_command_line']==True):
        return file_name[1:]

    return file_name

@api.route("/get_sim_result")
def get_sim_result():

    logger.info("Received simulation result get request.")

    tree_name=None
    event_type=None
    try:
        tree_name=request.args.get("tree_name")
        event_type=request.args.get("event_type")
    except Exception as ex:
        pass

    if(tree_name==None):
        return Response("tree name not mentioned", 400)

    logger.info("parameters: {}, {}".format(tree_name, event_type))
    sim_result = None
    try:
        # may use flask here with event id
        sim_result = db_utils.get_simulation_result(tree_name, event_type)
        #logger.info("simulation result: {}".format(jsonify(sim_result)))
        #TODO: later
        #tree_model = db_utils.get_tree_model()
    except Exception as ex:
        return Response(str(ex), 400)

    return Response(response = json.dumps(sim_result), status=200, content_type="application/json")

@api.route("/get_tree_details")
def get_tree_details():

    logger.info("Received tree details get request.")

    tree_name=None
    try:
        tree_name=request.args.get("tree_name")
    except Exception as ex:
        pass

    if(tree_name==None):
        return Response("tree name not mentioned", 400)

    logger.info("parameters: {}".format(tree_name))
    tree_details = dict()
    try:
        result = db_utils.get_tree_by_name(tree_name)
        tree_details['attackTree_ID']=result[1]
        tree_details['root']=json.loads(result[2].replace("'","\""))
        tree_details['subgoals']=json.loads(result[3].replace("'","\""))
        tree_details['leaf_nodes']=json.loads(result[4].replace("'","\""))
        tree_details['edges']=json.loads(result[5].replace("'","\""))
        tree_details['transitions']=json.loads(result[6].replace("'","\""))
    except Exception as ex:
        return Response(str(ex), 400)

    return Response(response = json.dumps(tree_details), status=200, content_type="application/json")

@api.route("/flush_cache")
def flush_cache():
    logger.info("received cache flush request")
    try:
        cache_utils.flush_all_data()
    except Exception as ex:
        return Response(str(ex), 400)

    return Response(response = "{\"msg\":\"cache flushed successfully.\"}", \
                                    status=200, content_type="application/json")

import functools
import time
import logging

logger=logging.getLogger('simulator_logger')

def timer(func):
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time   
        logger.info("Finished [{}] in run_time:{} secs".format(str(func.__name__), run_time))
        return value
    return wrapper_timer
PYTHON=python3
PIP=pip3
MAIN=src/app.py

.PHONY:	all install

all:
	$(PYTHON) $(MAIN)

install:	src/requirements.txt
	$(PIP) install -r src/requirements.txt

build:	Dockerfile
	sudo docker build -t cosim .

run:
	sudo docker run --rm -p 8080:8080 -v ${PWD}/json_files:/usr/cosim/json_files -v ${PWD}/figures:/usr/cosim/figures cosim

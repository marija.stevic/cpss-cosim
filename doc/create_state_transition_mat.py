import json

at_file = "AttackTree_EZ.json"
at_tree = None

def is_leaf_node(node_id):
    for leaf in at_tree["leaf_nodes"]:
        if node_id==leaf["node_ID"]:
            return True

    return False

   
with open(at_file) as file:
    at_tree = json.load(file)

at_tree=at_tree["attackModel"][0]

node_list=["leaf"]

for subgoal in at_tree["subgoal_nodes"]:
    node_list.append(subgoal["node_ID"])

node_list.append(at_tree["root"]["node_ID"])

at_tree["nodes"]=node_list

state_transitions=dict()
#1 based index
for trans in at_tree["transitions"]:
    output_index=1+node_list.index(trans["outputPlaces"][0])
    leaf_nodes=list()
    subgoal_trans=False
    for input in trans["inputPlaces"]:
        input_index=0
        if is_leaf_node(input):
            leaf_nodes.append(input)

    for input in trans["inputPlaces"]:

        if input in leaf_nodes:
            continue

        # for all subgoals at input place add individual entries, multiply probability of leaf nodes
        input_index=1+node_list.index(input)
        state_trans_index="(" + str(input_index) + "," + str(output_index) + ")"
        state_trans=list()
        state_trans.append(1/trans["compromise_t"])
        if leaf_nodes:
            state_trans.append([leaf_nodes])
        else:
            state_trans.append(["X"])

        if state_trans_index not in state_transitions:
            state_transitions[state_trans_index]=list()        
        state_transitions[state_trans_index].append(state_trans)
        subgoal_trans=True

    # only if all input places are leaf nodes
    if leaf_nodes and not subgoal_trans:
        state_trans_index="(1," + str(output_index) + ")"
        state_trans=list()
        state_trans.append(1/trans["compromise_t"])
        # not extend
        state_trans.append(leaf_nodes)

        if state_trans_index not in state_transitions:
            state_transitions[state_trans_index]=list()        
        state_transitions[state_trans_index].append(state_trans)

at_tree["state_transitions"]=state_transitions

with open(at_file[:-5] + "_processed.json", "w") as file:
    json.dump(at_tree, file, indent="\t")
FROM python

# install graphviz
RUN apt-get update -qq
RUN apt-get install graphviz -y
RUN rm -r /var/lib/apt/lists/*

WORKDIR /usr/cosim

# install python requirements (first for caching)
ADD ./src/requirements.txt ./src/
RUN pip install --no-cache-dir -r /usr/cosim/src/requirements.txt

# add application-files
ADD ./src ./src

# add entrypoint
ADD ./docker-entrypoint.sh ./

# add I-/O-Directories...
ADD ./json_files ./json_files
ADD ./figures ./figures
# ...and expose them as volumes
VOLUME ./json_files
VOLUME ./figures

# expose port for HTTP-API
EXPOSE 8080

ENTRYPOINT gunicorn -w 4 -b 0.0.0.0:8080 --chdir ./src wsgi:app --preload


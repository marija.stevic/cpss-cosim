# Persistence layer design
In order to persist data for cosimulator we will use **Redis** as cache.  

## Cache design  
In cache we will maintain our current collection of events and correlated events for all trees. 
Also we keep aggregated state marking for all trees in cache. These marking takes up most of the cache entries.

**EVENT_EXPIRY_TIME** can be configured from configuration file or from env file.  

**Entries to be maintained**

Key | Value | Purpose | Comments
----|----|----|----|
|<pre>event_TREEID_(F7/F8)_EVENTID</pre> | node name or id | storing events | Insertion: when request arrives after initial validation i.e.   valid node id in tree before new thread for simulation is spawned,   SETEX __EVENT_EXPIRY_TIME__ ex: SETEX mykey 300 "Hello"  
|<pre>event_DET_TREEID_(F7/F8)_EVENTID</pre> | whole request as JSON | storing events | same as above   |
|<pre>event_TREEID_corr_event_NODEID</pre> | node probability | fetch most recent node probability from cache | for subgoals probability is 1.0 |
|<pre>TREEID_STATEMARKING</pre> | index in aggregated states vector | This state marking are accessed once for each request in order to initialize initial state vector for CTMC simulation. |example: key: X1_11**010, value: 5, where X1 is tree id, ```11**010``` is state marking | 

_EVENTID_ refers to id parameter in event requests.  
Example: Key: ```event_DET_T2_F7_T2.3.2_201```, Value: whole request body as JSON string. Here EVENTID is 201.  

_TREEID_ is attackTreeId as per attack model. In above case it is ```T2```.  
_NODEID_ is nodeId parameter as per attack model for each node e.g. T2_0 for root it T2 tree.  

# Persistence layer implementation

Here we discuss how cache services are configured, initialized and finally run.  

## Cache

Service name: `cache-cpss-cosim`  

**External client usage:** redis-cli may be installed in local system. Then we can use ```redis-cli -p 6380``` and ```keys event_TREEID_F*``` to get all events.  

**Source location:**  
In directory src/cacheaccess as cache_props.py, responsible for storing environment variables and cache_utils.py. cache_utils.py provides the functions to populate events details and occurring node.  
  
_NOTE:_ By default keys have a lifetime of 300 seconds which is configurable from external env file and internal config.json

## Additional Endpoints  
We also need additional end points to access data from the web api interface. This is needed to get simulation result and attack tree details or flush cache contents.

Endpoint | Purpose | Parameters | Example | 
---------|---------|-----------|----------|  
api/flush_cache|Clear cache contents|  None| ```curl http://localhost:8080/api/flush_cache```|


## Running file in Linux (Ubuntu)

#### Using docker-compose (expected and easiest way)

1. Install Docker community edition
2. Install docker-compose
3. Clone the repository in a empty directory, let us call it `parent directory`
4. Go in the `copy_in_parent` directory and copy the contents in `parent directory`
5. Open a terminal in `parent directory`
6. Run the command `docker-compose up`, note based on your installation in first two steps sudo level access might be needed
7. This will do the following steps during first time,  
  a. Download Redis docker images  
  b. Build cosimulator from the respective Dockerfile in repository  
8. Note that during first time run,  
  a. You might see errors that cache connection failed. Ignore them and let docker run.    
  b. Cached data in Redis will be discarded immediately after service has been stopped  
9. To stop the services use command `docker-compose down`
10. If you are trying to run the service a see an error regarding volume should be unique, do step 9.
11. To force docker to rebuild docker images use `docker-compose up --build`, this is helpful if some code change has been occured for cosimulator
12. To inspect the cache state you may install redis-cli, follow the instructions in step 2 of standalone run.

#### Standalone run

1. Install Redis: https://redis.io/topics/quickstart
2. Run redis server by using `redis-server` command on terminal, this may also need sudo access and 
  the terminal needs to be open during cosimulator run or the command may be run in nohup mode
3. Test redis server by running `redis-cli ping` in separate window
4. To flush the cache you may use `flushall` in redis-cli
5. Use pip3 to install dependencies mentioned in `src/requirements.txt`
6. Now you can run standalone Gunicorn server or Flask process as per your preference

#### Debugging

1. To check status of cache redis-cli may be used  
   a. For docker-compose run, to install redis-cli only use `sudo apt-get install redis-tools`  
   b. For standalone run, during initial Redis installation redis-cli is also installed  
   c. Run `redis-cli -p 6380` to connect to redis database in docker service  
   d. Run `redis-cli` for standalone  
   e. To get all keys use `keys *` command and to clear cache use   
2. There is also an additional service called `test-server`. 
 This may be used to receive f6 responses. It logs and keeps track of received f6(s).  
   a. To use this append these lines to docker-compose.yml. Be careful with extra tabs in YAML format otherwise docker will throw error.  
        i.      test-server:  
        ii.         build: ./pn-cpss-cosim/test_server  
        iii.       ports:  
        iv.              - "9000:9000"  
   b. This receiver endpoint for f6 is default in both config.json and .env file  
   c. To check no of response send out do a GET request to http://test-server:9000/test_server/count.
      This only returns the count as integer.

## Running file in Windows

#### Using docker-compose

1. To install docker in windows install docker toolbox(https://docs.docker.com/toolbox/toolbox_install_windows/) which 
installs both docker CE and docker-compose in windows. 
2. After that execute step 3-5 as mentioned in Linux counterpart. 
3. Now modify the filepaths to meets windows format
4. If step 3 has been executed properly, step 6 and onwards should work the same manner from Linux counterpart.

#### Standalone run Windows

1. Install Redis: https://redislabs.com/ebook/appendix-a/a-3-installing-on-windows/a-3-2-installing-redis-on-window/ 
  This page contains instructions of running Redis too.
2. Install Python 3, if not done already
3. Use pip to install dependencies mentioned in `src/requirements.txt`
4. Now you can run standalone Flask process
5. Note here by default Flask server is single threaded, so to test chained occurence, hit the endpoint (/f7 or /f8) wait 
  for the execution to complete and then hit again. As each event stays in cache for 5 minutes by default as mentioned in config.json, 
  this should be enough for debug purpose.





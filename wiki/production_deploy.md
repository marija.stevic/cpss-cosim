Production deployment guide:
----------------------------

CPSS Co-simulator module is made to be deployable as easily as possible with
minimal configuration. It can be ran as docker container or a python application
from command line. Here we discuss the relevant steps to the deployment.

## Setting up the application

1. Clone or download and extract whole contents of the repository  
2. Copy contents of the `copy_in_parent` folder in the same folder
3. If step two is not applicable i.e. there is already an docker_compose.yml file, then copy the services to the same file
4. In case step 3 was executed, either all_configs.env needs to be copied in the same directory or content of the same needs to be copied to appropaite .env file
5. Make sure **cosimulator__props_f7__out_endpoint** and **cosimulator__props_f8__out_endpoint** is updated to reflect the endpoint to which f6 response should be sent
6. Please note build path towards Dockerfile may need to be updated based on relative location of docker-compose.yml file
7. Same is applicable for the .env file, currently pn-cpss-cosim refers to all_configs.env
8. Update the outbound ports as needed. Currently 8080 and 6380 ports are being used. Please do not update mysql and redis inbound ports. 
In case of update in inboind port of main service pn-cpss-cosim, refer the port_details section below.

## Configuration changes:

**Configuration file location**: "all_configs.env"

_OPTIONAL:_ In the same directory, there is a config file for logging configuration: "json_files/configs/log_config.json".
Here __log_file_name__ property defines the location and name of log file. Currently it points to project base
directory and may be modified as needed. But please be careful it should be pointing
to an existing directory. Also in docker container this path should be valid.

## Running the application:

This application should be run as docker-compose application. It will spawn db-cpss-cosim and cache-cpss-cosim service first.
Then start the pn-cpss-cosim service i.e. the main application.

## Specifications:

It should be noted that unlike other web application Cosimulator is responsible for
running statistical simulation hence it expects higher amount of resources. It should
be assigned **at least 2GB RAM, 2 cores and 500MB of storage**. Storage is requirred as we
are tracking every simulation by writing them to file system. The _simulation results_ are
written to 'json_files/output_files/' directory in project base location. It is mounted as volume in
DockerFile. If needed it may be changed to an external volume.

This is ran inside Gunicorn web server with multiple workers. One important note
should be made here as this is a CPU bound job so no of
workers should not be more than no of cores assigned to python process or docker
container. This number of workers can be modified my editing by '-w' parameter in
Gunicorn command as _ENTRYPOINT_ value in DockerFile.

## Port details:

Currently this module expects input from **8080** port. This can be changed by
modifiying the _ENTRYPOINT_ value in DockerFile or modifying the Gunicorn command or
modifying the last line in src/app.py i.e. change value 8080 to expected port value.
